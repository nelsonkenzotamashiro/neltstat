package com.example.android.neltstat.RecyclerViewAdapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.neltstat.R;
import com.example.android.neltstat.database.Entities.ActiveConnections;
import com.example.android.neltstat.netstat.Intents.PROTOCOL;
import com.example.android.neltstat.utils.TimeUtils;

import java.util.List;

public class ActiveConnectionsRecyclerViewAdapter extends RecyclerView.Adapter<ActiveConnectionsRecyclerViewAdapter.ActiveConnectionsViewHolder> {


    private List<ActiveConnections> activeConnectionsArrayList;

    @NonNull
    @Override
    public ActiveConnectionsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.active_connections, viewGroup, false);
        ActiveConnectionsViewHolder connectionViewHolder = new ActiveConnectionsViewHolder(view);
        return connectionViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ActiveConnectionsViewHolder connectionViewHolder, int i) {
        connectionViewHolder.set(activeConnectionsArrayList.get(i));
    }

    @Override
    public int getItemCount() {
        if(activeConnectionsArrayList ==null) return 0;
        return activeConnectionsArrayList.size();
    }

    public void setConnectionArrayList(List<ActiveConnections> connectionArrayList) {
        this.activeConnectionsArrayList = connectionArrayList;
        notifyDataSetChanged();
    }

    class ActiveConnectionsViewHolder extends RecyclerView.ViewHolder{

        private TextView
                appUID_textView,
                localInfo_textView,
                remoteInfo_textView,
                hangingtime_textView,
                protocol_textView,
                stage1_textView,
                stage2_textView,
                stage3_textView,
                stage4_textView;

        public ActiveConnectionsViewHolder(@NonNull View itemView) {
            super(itemView);
            appUID_textView      = itemView.findViewById(R.id.app_uid       );
            localInfo_textView   = itemView.findViewById(R.id.local_info    );
            remoteInfo_textView  = itemView.findViewById(R.id.port_ip_info);
            protocol_textView    = itemView.findViewById(R.id.protocol_info );
            hangingtime_textView = itemView.findViewById(R.id.hanging_time  );
            stage1_textView      = itemView.findViewById(R.id.stage_1       );
            stage2_textView      = itemView.findViewById(R.id.stage_2       );
            stage3_textView      = itemView.findViewById(R.id.stage_3       );
            stage4_textView      = itemView.findViewById(R.id.stage_4       );
        }

        public void set(ActiveConnections activeConnections){
            appUID_textView     .setText(""+activeConnections.getUid());
            localInfo_textView  .setText(activeConnections.getLocal_ip().getHostAddress()+":"+ activeConnections.getLocal_port());
            remoteInfo_textView .setText(activeConnections.getRemote_ip().getHostAddress()+":"+ activeConnections.getRemote_port());
            hangingtime_textView.setText(TimeUtils.timeDiff(activeConnections.getFirst_seen(), activeConnections.getLast_seen()));
            protocol_textView   .setText(PROTOCOL.NAME[activeConnections.getProtocol()]);
            if (activeConnections.did_stage_1()) stage1_textView.setVisibility(View.VISIBLE);
            if (activeConnections.did_stage_2()) stage2_textView.setVisibility(View.VISIBLE);
            if (activeConnections.did_stage_3()) stage3_textView.setVisibility(View.VISIBLE);
            if (activeConnections.did_stage_4()) stage4_textView.setVisibility(View.VISIBLE);
        }

//        @Override
//        public void onClick(View v) {
//            connectionsClickInterface.onClick(activeConnectionsArrayList.get(getAdapterPosition()).getRemote_ip().getHostAddress());
//        }

    }


}
