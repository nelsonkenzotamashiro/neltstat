package com.example.android.neltstat;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.android.neltstat.RecyclerViewAdapters.ApplicationsRecyclerViewAdapter;
import com.example.android.neltstat.RecyclerViewAdapters.ActiveConnectionsRecyclerViewAdapter;
import com.example.android.neltstat.RecyclerViewAdapters.OrganizationRecyclerViewAdapter;
import com.example.android.neltstat.database.Entities.ActiveConnections;
import com.example.android.neltstat.database.Entities.Applications;
import com.example.android.neltstat.database.Entities.Organizations;
import com.example.android.neltstat.database.NeltstatDatabase;

import java.util.List;

public class OrganizationsListView extends AppCompatActivity {

    private RecyclerView recyclerView;
    private OrganizationRecyclerViewAdapter organizationRecyclerViewAdapter;
    private ApplicationsRecyclerViewAdapter applicationsRecyclerViewAdapter;
    private ActiveConnectionsRecyclerViewAdapter activeConnectionsRecyclerViewAdapter;
//    private NeltstatDatabase neltstatDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        recyclerView = findViewById(R.id.database_recycler_view);

        setAdapters();

        recyclerView.setAdapter(organizationRecyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.database_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_select_applications:
                recyclerView.setAdapter(applicationsRecyclerViewAdapter);
                break;
            case R.id.action_select_organizations:
                recyclerView.setAdapter(organizationRecyclerViewAdapter);
                break;
            case R.id.action_select_active_connections:
                recyclerView.setAdapter(activeConnectionsRecyclerViewAdapter);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setAdapters(){

        applicationsRecyclerViewAdapter = new ApplicationsRecyclerViewAdapter(this.getApplicationContext());
        LiveData<List<Applications>> appListLiveData = NeltstatDatabase
                .InstanceOf(this.getApplicationContext()).applicationDAO().load_all();
        appListLiveData.observe(this, new Observer<List<Applications>>() {
            @Override
            public void onChanged(@Nullable List<Applications> applicationsList) {
                applicationsRecyclerViewAdapter.setApplicationsArrayList(applicationsList);
            }
        });

        organizationRecyclerViewAdapter = new OrganizationRecyclerViewAdapter();
        LiveData<List<Organizations>> orgListLiveData = NeltstatDatabase
                .InstanceOf(this.getApplicationContext()).organizationDAO().load_all();
        orgListLiveData.observe(this, new Observer<List<Organizations>>() {
            @Override
            public void onChanged(@Nullable List<Organizations> organizationsList) {
                organizationRecyclerViewAdapter.setOrgSummaryList(organizationsList);
            }
        });

        activeConnectionsRecyclerViewAdapter = new ActiveConnectionsRecyclerViewAdapter();
        LiveData<List<ActiveConnections>> conListLiveData = NeltstatDatabase
                .InstanceOf(this.getApplicationContext()).activeConnectionsDAO().load_all();
        conListLiveData.observe(this, new Observer<List<ActiveConnections>>() {
            @Override
            public void onChanged(@Nullable List<ActiveConnections> activeConnectionsList) {
                activeConnectionsRecyclerViewAdapter.setConnectionArrayList(activeConnectionsList);
            }
        });

    }

}
