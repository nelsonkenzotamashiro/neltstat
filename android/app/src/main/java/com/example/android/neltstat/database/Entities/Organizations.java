package com.example.android.neltstat.database.Entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.android.neltstat.whois.datatype.OrgSummary;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

@Entity(
        tableName = "organizations",
        indices = @Index(value = {"min_ip", "max_ip"}, unique = true)
)
public class Organizations {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int org_id;
    private String      org_name;
    private String      net_name;
    private String      net_type;
    private InetAddress min_ip;
    private InetAddress max_ip;
    private String      country;
    @ColumnInfo(name = "contact")
    private String contact_email;
    @ColumnInfo(name = "abuse")
    private String abuse_email;
    private Date    last_seen;
    private String  trust_state;

    public Organizations(
            @NonNull int org_id,
            String org_name,
            String net_name,
            InetAddress min_ip,
            InetAddress max_ip,
            String net_type,
            String country,
            String contact_email,
            String abuse_email,
            Date last_seen,
            String trust_state
    ) {
        this.org_id = org_id;
        this.org_name = org_name;
        this.net_name = net_name;
        this.min_ip = min_ip;
        this.max_ip = max_ip;
        this.net_type = net_type;
        this.country = country;
        this.contact_email = contact_email;
        this.abuse_email = abuse_email;
        this.last_seen = last_seen;
        this.trust_state = trust_state;
    }

    @Ignore
    public Organizations(OrgSummary orgSummary, Date ref_date){
        this.org_name       = orgSummary.getOrgName();
        this.net_name       = orgSummary.getNetName();
        try {
            this.min_ip         = InetAddress.getByName(orgSummary.getMinIP());
            this.max_ip         = InetAddress.getByName(orgSummary.getMaxIP());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.net_type       = orgSummary.getNetType();
        this.country        = orgSummary.getCountry();
        this.contact_email  = orgSummary.getEmailContact();
        this.abuse_email    = orgSummary.getEmailAbuse();
        this.last_seen      = ref_date;
        this.trust_state    = "not defined";
    }

    @NonNull
    public int getOrg_id() {
        return org_id;
    }
    public String getOrg_name() {
        return org_name;
    }
    public String getNet_name() {
        return net_name;
    }
    public InetAddress getMin_ip() {
        return min_ip;
    }
    public InetAddress getMax_ip() {
        return max_ip;
    }
    public String getNet_type() {
        return net_type;
    }
    public String getCountry() {
        return country;
    }
    public String getContact_email() {
        return contact_email;
    }
    public String getAbuse_email() {
        return abuse_email;
    }
    public Date getLast_seen() {
        return last_seen;
    }
    public String getTrust_state() {
        return trust_state;
    }

}