package com.example.android.neltstat.netstat.utils;

import com.example.android.neltstat.netstat.Intents.PROTOCOL;
import com.example.android.neltstat.netstat.datatype.Connection;
import com.example.android.neltstat.netstat.datatype.RawConnectionInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;


/**
 *  SystemRuntimeCaller is an Static Adapter of System's Runtime interface to Netstat
 *      - uses Runtime to run 'cat /proc/net/(tcp/tcp6/udp/udp6)' and gather raw information
 *      - returns interpreted information in RawConnectionInfo data
 * */
public class SystemRuntimeCaller {

    public static ArrayList<Connection> getRichConnections(int protocol) {
        try {
            ArrayList<Connection> connectionArrayList = toRichInfo(toRawInfo(callRuntime(protocol), protocol));
//            Log.d("NELSON", "quantity active_connections="+connectionArrayList.size());
            return connectionArrayList;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<RawConnectionInfo> getAllRawProtocols(){
        ArrayList<RawConnectionInfo> rawConnectionInfoArrayList = new ArrayList<>();
        rawConnectionInfoArrayList.addAll(getRawConnections(PROTOCOL.TCP4));
        rawConnectionInfoArrayList.addAll(getRawConnections(PROTOCOL.TCP6));
        rawConnectionInfoArrayList.addAll(getRawConnections(PROTOCOL.UDP4));
        rawConnectionInfoArrayList.addAll(getRawConnections(PROTOCOL.UDP6));
        return rawConnectionInfoArrayList;
    }

    public static ArrayList<RawConnectionInfo> getRawConnections(int protocol) {
        try {
            return toRawInfo(callRuntime(protocol), protocol);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static ArrayList<Connection>  toRichInfo(ArrayList<RawConnectionInfo> rawConnectionInfoArrayList){
        ArrayList<Connection> connectionArrayList = new ArrayList<>();
        Iterator<RawConnectionInfo> rawConnectionInfoIterator = rawConnectionInfoArrayList.iterator();
        while(rawConnectionInfoIterator.hasNext()){
            RawConnectionInfo rawConnectionInfo = rawConnectionInfoIterator.next();
            try {
                Connection connection = new Connection.Builder()
                        .set_clientRawIP    (rawConnectionInfo.getLocalIP()    )
                        .set_clientRawPort  (rawConnectionInfo.getLocalPort()  )
                        .set_serverRawIP    (rawConnectionInfo.getRemoteIP()    )
                        .set_serverRawPort  (rawConnectionInfo.getRemotePort()  )
                        .set_rawUid         (rawConnectionInfo.getUid()         )
                        .set_rawSeenState   (rawConnectionInfo.getStateCode()   )
                        .set_protocol       (rawConnectionInfo.getProtocol()    )
                        .build();
//                connection.logInfo_simple();
                connectionArrayList.add(connection);
            } catch (UnknownHostException e) {
                e.printStackTrace();
                continue;
            }
        }
        return connectionArrayList;
    }

    private static ArrayList<RawConnectionInfo> toRawInfo(BufferedReader bufferedReader, int protocol){
        ArrayList<RawConnectionInfo> rawConnectionInfoArrayList = new ArrayList<>();
        String line;
        try {
            bufferedReader.readLine();
            while( (line = bufferedReader.readLine()) != null){
                String[] aux_list = Arrays.copyOfRange(
                        line.trim()
                                .replaceAll(":"," ")
                                .replaceAll(" +"," ")
                                .split(" "), 1, 14);
                rawConnectionInfoArrayList.add(
                        new RawConnectionInfo.Builder()
                                .set_localIP(aux_list[ 0])  // array position to clientIP
                                .set_localPort(aux_list[ 1])  // array position to clientPort
                                .set_remoteIP(aux_list[ 2])  // array position to serverIP
                                .set_remotePort(aux_list[ 3])  // array position to serverPort
                                .set_stateCode  (aux_list[ 4])  // array position to stateCode
                                .set_uid        (aux_list[10])  // array position to uid
                                .set_protocol   (protocol    )  // protocol (tcp/udp/tcp6/udp6)
                                .build()
                );
            }
            return rawConnectionInfoArrayList;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static BufferedReader callRuntime(int protocol) throws IOException {
        String [] cmd = null;
        switch (protocol){
            case PROTOCOL.TCP4:
                cmd = new String[]{"cat", "/proc/net/tcp"};
                break;
            case PROTOCOL.TCP6:
                cmd = new String[]{"cat", "/proc/net/tcp6"};
                break;
            case PROTOCOL.UDP4:
                cmd = new String[]{"cat", "/proc/net/udp"};
                break;
            case PROTOCOL.UDP6:
                cmd = new String[]{"cat", "/proc/net/udp6"};
                break;
        }
        if (cmd == null) return null;
        Process process = Runtime.getRuntime().exec(cmd);
        return new BufferedReader(
                new InputStreamReader(
                        process.getInputStream()));
    }

}
