package com.example.android.neltstat.whois.Utils;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.android.neltstat.whois.Intents.HOSTS;
import com.example.android.neltstat.whois.Intents.TAGS;

import org.apache.commons.net.util.SubnetUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Interpreter {

//    @RequiresApi(api = Build.VERSION_CODES.N)
//    private static ArrayList<Integer> find_tags(
//            ArrayList<String> response,
//            String[]... patternSets
//    ){
//        HashSet<Integer> integerHashSet = new HashSet<>();
//        for (int i=0; i<response.size(); i++){
//            // include if patterns match at at least one pattern in each patternSet
//            Boolean include = true;
//            for (String[] patternSet: patternSets){
//                // checks whether patternSet matches at least once
//                Boolean one_pattern_match = false;
//                for (String pattern : patternSet){
//                    if (Pattern.compile(":").matcher(response.get(i)).find()){
//                        if (Pattern
//                                .compile(pattern, Pattern.CASE_INSENSITIVE)
//                                .matcher(response.get(i))
//                                .find()
//                                ) one_pattern_match = true;
//                    }
//                }
//                // if not at least one pattern matches, do not include
//                if (!one_pattern_match) include = false;
//            }
//            if (include) integerHashSet.add(i);
//        }
//
//        ArrayList<Integer> integerArrayList = new ArrayList<>();
//        integerArrayList.addAll(integerHashSet);
//        integerArrayList.sort(new Comparator<Integer>() {
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                return o1-o2;
//            }
//        });
//
//        return integerArrayList;
//    }

    private static ArrayList<String> find_tag(ArrayList<String> list, String[]... patternSets){
        ArrayList<String> aux_list = new ArrayList<>();
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()){
            String line = iterator.next();
            if (has_sepparatorAtLine(line))
                if (do_match(get_tagAtLine(line), patternSets))
                    aux_list.add(line);
        }
        return aux_list;
    }

    private static ArrayList<String> intersect(ArrayList<String> list, String[]...patterns){
        ArrayList<String> aux_list = new ArrayList<>();
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()){
            String line = iterator.next();
            if (do_match(line, patterns)) aux_list.add(line);
        }
        return aux_list;
    }

    private static ArrayList<String> filter(ArrayList<String> list, String[]...patterns){
        ArrayList<String> aux_list = new ArrayList<>();
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()){
            String line = iterator.next();
            if (!do_match(line, patterns)) aux_list.add(line);
        }
        return aux_list;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private static ArrayList<String> dePriorityze(ArrayList<String> list, final String[]...patterns){
        list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (do_match(o1,patterns)) if( !do_match(o2,patterns)) return 1;
                return -1;
            }
        });
        return list;
    }

    private static Boolean do_match(String line, String[]...patternSet){
        for (String[] pattern : patternSet){
            for (String word : pattern) {
                if (Pattern.compile(word, Pattern.CASE_INSENSITIVE).matcher(line).find()) return true;
            }
        }
        return false;
    }

    // TODO(014): Implement support to attributes presented in more than one line

    private static Boolean has_sepparatorAtLine(String line){
        if (!Pattern.compile(":").matcher(line).find()) return false;
        String[] splitted = line.split(":");
        for (String word : splitted) if (word.trim().replace(" +", "").length() == 0 ) return false;
        return true;
    }

    private static String get_tagAtLine(String line){
        if (has_sepparatorAtLine(line)) return line.split(":")[0].trim().replace(" +", "");
        return null;
    }

    private static String get_attributeAtLine(String line){
        if (has_sepparatorAtLine(line)) return line.split(":")[1].replace(" +", "").trim();
        return null;
    }

    private static ArrayList<String> get_ipsFromRange(String line){
        ArrayList<String> iplist = new ArrayList<>();
        String new_regex    = "((25[0-5])|(2[0-4]\\d)|([0-1]?\\d?\\d))";
        String ip_regex = "("+new_regex+")"+"(\\."+new_regex+"){3}";
        Matcher matcher = Pattern.compile(ip_regex).matcher(line);
        while (matcher.find()) iplist.add(matcher.group());
        return iplist;
    }

    private static ArrayList<String> get_ipsFromCIDR(String line){

        ArrayList<String> iplist = new ArrayList<>();
        String regex_0_255 = "((25[0-5])|(2[0-4]\\d)|([0-1]?\\d?\\d))";
        String regex_0_32  = "((3[0-2])|([0-2]?\\d))";
        String ip_regex = "("+regex_0_255+")"+"(\\."+regex_0_255+"){3}"+"/"+regex_0_32;
        Matcher matcher = Pattern.compile(ip_regex).matcher(line);

        if (matcher.find()){
            SubnetUtils subnetUtils = new SubnetUtils(matcher.group());
            subnetUtils.setInclusiveHostCount(true);
            SubnetUtils.SubnetInfo subnetInfo = subnetUtils.getInfo();

            iplist.add(subnetInfo.getLowAddress());
            iplist.add(subnetInfo.getHighAddress());
        }
        return iplist;

    }

    private static Boolean has_cidr(String line){
        String regex_0_255 = "((25[0-5])|(2[0-4]\\d)|([0-1]?\\d?\\d))";
        String regex_0_32  = "((3[0-2])|([0-2]?\\d))";
//        String ip_regex = "("+regex_0_255+")"+"(\\."+regex_0_255+"){3}";
        String net_regex = "("+regex_0_255+")"+"(\\."+regex_0_255+"){3}"+"/"+regex_0_32;

        if (Pattern.compile(net_regex).matcher(line).find()) return true;
        return false;
    }

    private static Boolean has_ipsRange(String line){
        String regex_0_255 = "((25[0-5])|(2[0-4]\\d)|([0-1]?\\d?\\d))";
        String regex_0_32  = "((3[0-2])|([0-2]?\\d))";
        String ip_regex = "("+regex_0_255+")"+"(\\."+regex_0_255+"){3}";
//        String net_regex = "("+regex_0_255+")"+"(\\."+regex_0_255+"){3}"+"/"+regex_0_32;

        int ips_count=0;
        Matcher matcher = Pattern.compile(ip_regex).matcher(line);
        while (matcher.find()) ips_count++;
        if (ips_count==2) return true;
        return false;
    }

    public static String get_orgName(ArrayList<String> response){
        ArrayList<String> match_indexes = find_tag(response, TAGS.ORG_NAME);
        if (match_indexes.size() == 0) return null;
        return get_attributeAtLine(match_indexes.get(0));
    }
    public static String get_netName(ArrayList<String> response){
        ArrayList<String> match_indexes = find_tag(response, TAGS.NETNAME);
        if( match_indexes.size() == 0 ) return null;
        return get_attributeAtLine(match_indexes.get(0));
    }
    public static String get_minIP(ArrayList<String> response){
        ArrayList<String> match_indexes = find_tag(response, TAGS.IP_RANGE);
        if( match_indexes.size() == 0 ) return null;
        String line = match_indexes.get(0);
        ArrayList<String> iplist = new ArrayList<>();
        if (has_cidr    (line)) iplist = get_ipsFromCIDR (line);
        if (has_ipsRange(line)) iplist = get_ipsFromRange(line);
        if (!iplist.isEmpty()) return iplist.get(0);
        return "";
    }
    public static String get_maxIP(ArrayList<String> response){
        ArrayList<String> match_indexes = find_tag(response, TAGS.IP_RANGE);
        if( match_indexes.size() == 0 ) return null;
        String line = match_indexes.get(0);
        ArrayList<String> iplist = new ArrayList<>();
        if (has_cidr    (line)) iplist = get_ipsFromCIDR (line);
        if (has_ipsRange(line)) iplist = get_ipsFromRange(line);
        if (!iplist.isEmpty()) return iplist.get(1);
        return "";
    }
    public static String get_netType(ArrayList<String> response){
        ArrayList<String> match_indexes = find_tag(response, TAGS.NET_TYPE);
        if( match_indexes.size() == 0 ) return null;
        return get_attributeAtLine(match_indexes.get(0));
    }
    public static String get_country(ArrayList<String> response){
        ArrayList<String> match_indexes = find_tag(response, TAGS.COUNTRY);
        if( match_indexes.size() == 0 ) return null;
        return get_attributeAtLine(match_indexes.get(0));
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String get_emailContact(ArrayList<String> response){
        ArrayList<String> match_indexes = find_tag    (response, TAGS.CONTACT);
        match_indexes                   = dePriorityze(match_indexes, TAGS.ABUSE);
        if( match_indexes.size() == 0 ) return null;
        return get_attributeAtLine(match_indexes.get(0));
    }
    public static String get_emailAbuse(ArrayList<String> response){
        ArrayList<String> match_indexes = find_tag(response, TAGS.CONTACT);
        match_indexes                   = intersect(match_indexes, TAGS.ABUSE);
        if( match_indexes.size() == 0 ) return null;
        return get_attributeAtLine(match_indexes.get(0));
    }
    public static String get_refer(ArrayList<String> response){
        ArrayList<String> match_indexes = find_tag(response, TAGS.REFER);
        if( match_indexes.size() == 0 ) return null;
        return get_attributeAtLine(match_indexes.get(0));
    }

    public static boolean is_referValid(String refer){
        if (refer == null) return false;
        switch (refer){
            case HOSTS.IANA:
                return true;
            case HOSTS.LACNIC:
                return true;
            case HOSTS.RIPE:
                return true;
            case HOSTS.AFRINIC:
                return true;
            case HOSTS.ARIN:
                return true;
            case HOSTS.APNIC:
                return true;
        }
        return false;
    }

}
