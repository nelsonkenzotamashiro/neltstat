package com.example.android.neltstat;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.android.neltstat.database.Entities.Organizations;
import com.example.android.neltstat.database.NeltstatDatabase;
import com.example.android.neltstat.whois.Whois;
import com.example.android.neltstat.whois.datatype.OrgSummary;
import com.example.android.neltstat.whois.datatype.Response;
import com.google.common.net.InetAddresses;

import java.util.Date;

public class DetailedWhoisActivity
    extends AppCompatActivity
    implements LoaderManager.LoaderCallbacks<Whois> {

    public static final String TARGET_HOST_WHOIS = "target host whois information";
    private NeltstatDatabase neltstatDatabase;
    private OrgSummary      orgSummary;
    private Response        response;
    private TextView        textView;
    private int             WHOIS_QUERY_ID;
    private String          WHOIS_QUERY_BUNDLE_EXTRA;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.whois_detailed);
        textView = findViewById(R.id.whois_detailed_info_text_view);
        neltstatDatabase = NeltstatDatabase.InstanceOf(this.getApplicationContext());
        WHOIS_QUERY_ID = 3245;
        WHOIS_QUERY_BUNDLE_EXTRA = "whois query";

        Bundle bundle = new Bundle();
        bundle.putString(WHOIS_QUERY_BUNDLE_EXTRA, getIntent().getStringExtra(TARGET_HOST_WHOIS));
        LoaderManager loaderManager = getSupportLoaderManager();
        Loader<Whois> whoisLoader = loaderManager.getLoader(WHOIS_QUERY_ID);

        if ( whoisLoader == null ){
            loaderManager.initLoader(WHOIS_QUERY_ID, bundle, this);
        }else {
            loaderManager.restartLoader(WHOIS_QUERY_ID, bundle, this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.whois_detailed, menu);
        return true;
//        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_action_save_summary:
                new Thread(new InsertSummary(orgSummary)).start();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class InsertSummary implements Runnable{

        OrgSummary orgSummary;

        public InsertSummary(OrgSummary orgSummary) {
            this.orgSummary = orgSummary;
        }

        @Override
        public void run() {
            Organizations organizations = neltstatDatabase
                    .organizationDAO()
                    .load_singleByIP(
                            InetAddresses.decrement(
                                    InetAddresses.forString(orgSummary.getMaxIP())));
            if (organizations == null)  neltstatDatabase.organizationDAO().insert(new Organizations(orgSummary, new Date()));
            else                        neltstatDatabase.organizationDAO().update(new Organizations(orgSummary, new Date()));
        }

    }

    @NonNull
    @Override
    public Loader<Whois> onCreateLoader(int i, @Nullable final Bundle bundle) {
        return new AsyncTaskLoader<Whois>(this) {

            private Whois whois;

            @Override
            protected void onStartLoading() {
                if (whois != null) deliverResult(whois);
                else               forceLoad();
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Nullable
            @Override
            public Whois loadInBackground() {
                return new Whois(bundle.getString(WHOIS_QUERY_BUNDLE_EXTRA)).run();
            }

            @Override
            public void deliverResult(@Nullable Whois data) {
                whois = data;
                super.deliverResult(data);
            }
        };
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Whois> loader, Whois whois) {
        orgSummary = whois.getSummary();
        response   = whois.getLastResponse();
        textView.setText(response.toString());
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Whois> loader) {

    }

}
