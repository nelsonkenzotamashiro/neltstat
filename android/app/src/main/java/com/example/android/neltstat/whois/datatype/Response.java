package com.example.android.neltstat.whois.datatype;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.android.neltstat.whois.Utils.Interpreter;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Handler to whois responses
 *
 */
public class Response {

    private ArrayList<String> response;
    private OrgSummary        orgSummary;
    private String            refer;

    public Response(BufferedReader reader){
        response = new ArrayList<>();
        storeReader(reader);
    }

    private void storeReader(BufferedReader reader){
        String line;
        try{
            while ((line = reader.readLine()) != null) {
                response.add(line.trim());
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String get_referer(){
        if (refer!=null) return refer;
        String aux_refer = Interpreter.get_refer(response);
        if (Interpreter.is_referValid(aux_refer)) refer = aux_refer;
        return refer;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public OrgSummary get_summary(){
        if (orgSummary !=null) return orgSummary;
        orgSummary = new OrgSummary.Builder()
                .set_OrgName      (Interpreter.get_orgName      (response))
                .set_NetName      (Interpreter.get_netName      (response))
                .set_MinIP        (Interpreter.get_minIP        (response))
                .set_MaxIP        (Interpreter.get_maxIP        (response))
                .set_NetType      (Interpreter.get_netType      (response))
                .set_Country      (Interpreter.get_country      (response))
                .set_EmailContact (Interpreter.get_emailContact (response))
                .set_EmailAbuse   (Interpreter.get_emailAbuse   (response))
                .build();
        return orgSummary;
    }

    public ArrayList<String> asArrayList() {
        return response;
    }

    public String toString(){
        String text = "";
        Iterator<String> iterator = response.iterator();
        while (iterator.hasNext()) text += iterator.next()+"\n";
        return text;
    }

    public void log_response(){
        Iterator<String> responseIterator = response.iterator();
        while(responseIterator.hasNext()){
            Log.d("NELSON",responseIterator.next());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void log_summary(){
        Log.v("NELSON", get_summary().toString());
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void log_attributes(){
        Log.v("NELSON","" +
                Interpreter.get_orgName(response)                       + "\n" +
                Interpreter.get_netName(response)                       + "\n" +
                Interpreter.get_minIP(response)         + "-" +
                Interpreter.get_maxIP(response)                         + "\n" +
                Interpreter.get_netType(response)                       + "\n" +
                Interpreter.get_country(response)                       + "\n" +
                Interpreter.get_emailContact(response)                  + "\n" +
                Interpreter.get_emailAbuse(response)
        );
    }

}
