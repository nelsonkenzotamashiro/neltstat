package com.example.android.neltstat.database.Entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.IconCompat;

import com.example.android.neltstat.appInfoSummary.AppSummary;
import com.example.android.neltstat.netstat.utils.TypeTranslationUtils;

import java.util.Date;

@Entity(
        tableName = "applications",
        indices = @Index(value = "app_uid", unique = true)
)
public class Applications {

    @PrimaryKey (autoGenerate = true)
    @NonNull
    private int    app_id;
    private String app_name;
    private int    app_uid;
    private String app_fullname;
    private String version;
    private String category;
    private Date   last_seen;
    private String trust_state;

    @Ignore
    private Drawable app_icon;

    public Applications(
            @NonNull int app_id,
            String app_name,
            int app_uid,
            String app_fullname,
            String version,
            String category,
            Date last_seen,
            String trust_state
    ) {
        this.app_id = app_id;
        this.app_name = app_name;
        this.app_uid = app_uid;
        this.app_fullname = app_fullname;
        this.version = version;
        this.category = category;
        this.last_seen = last_seen;
        this.trust_state = trust_state;
    }

    @NonNull
    public int    getApp_id() {
        return app_id;
    }
    public String getApp_name() {
        return app_name;
    }
    public int getApp_uid() {
        return app_uid;
    }
    public String getCategory() {
        return category;
    }
    public String getApp_fullname() {
        return app_fullname;
    }
    public String getVersion() {
        return version;
    }
    public Date   getLast_seen() {
        return last_seen;
    }
    public String getTrust_state() {
        return trust_state;
    }
    public Drawable getApp_icon(){
        return app_icon;
    }

    public void setApp_icon(Drawable app_icon) {
        this.app_icon = app_icon;
    }

    public void update (Applications applications){
        if (applications.getApp_id      ()!=   0) app_id        = applications.getApp_id      ();
        if (applications.getApp_name    ()!=null) app_name      = applications.getApp_name    ();
        if (applications.getCategory    ()!=null) category      = applications.getCategory    ();
        if (applications.getApp_fullname()!=null) app_fullname  = applications.getApp_fullname();
        if (applications.getVersion     ()!=null) version       = applications.getVersion     ();
        if (applications.getLast_seen   ()!=null) last_seen     = applications.getLast_seen   ();
        if (applications.getTrust_state ()!=null) trust_state   = applications.getTrust_state ();
        if (applications.getApp_uid     ()!=   0) app_uid       = applications.getApp_uid     ();
        if (applications.getApp_icon    ()!=null) app_icon      = applications.getApp_icon    ();
    }

    @Ignore
    public Applications (AppSummary appSummary, Date ref_date){
        app_id          = 0;
        app_name        = appSummary.getName();
        app_fullname    = appSummary.getFullname();
        app_uid         = TypeTranslationUtils.uid(appSummary.getApp_uid());
        version         = appSummary.getVersion();
        category        = appSummary.getCategory();
        last_seen       = ref_date;
    }

}