package com.example.android.neltstat.whois.Intents;

public class HOSTS {

    public static final String IANA    = "whois.iana.org";
    public static final String LACNIC  = "whois.lacnic.net";
    public static final String ARIN    = "whois.arin.net";
    public static final String RIPE    = "whois.ripe.net";
    public static final String APNIC   = "whois.apnic.net";
    public static final String AFRINIC = "whois.afrinic.net";

}
