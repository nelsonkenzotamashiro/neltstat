package com.example.android.neltstat.database.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.android.neltstat.database.Entities.Applications;

import java.util.Date;
import java.util.List;

@Dao
public interface ApplicationsDAO {

    @Insert
    void insert(Applications...applications);
    @Insert
    void insert(List<Applications> applicationsList);

    @Update
    int update(Applications... applications);
    @Update
    void update(List<Applications> applicationsList);

    @Delete
    void delete(Applications...applications);
    @Delete
    void delete(List<Applications>applicationsList);

    @Query("select * from applications;")
    LiveData<List<Applications>> load_all();
    @Query("SELECT * FROM applications WHERE app_name = :name LIMIT 1;")
    Applications load_byName(String name);
    @Query("SELECT * FROM applications WHERE app_uid = :uid LIMIT 1;")
    Applications load_byUID(int uid);
    @Query("SELECT * FROM applications WHERE last_seen > :date;")
    List<Applications> load_sinceDate(Date date);
    @Query("SELECT * FROM applications WHERE trust_state = :state;")
    List<Applications> load_byTrustState(String state);

}