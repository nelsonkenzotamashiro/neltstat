package com.example.android.neltstat;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.android.neltstat.appInfoSummary.AppInfoRetriever;
import com.example.android.neltstat.database.Entities.ActiveConnections;
import com.example.android.neltstat.database.Entities.Applications;
import com.example.android.neltstat.database.Entities.ConnectionsResume;
import com.example.android.neltstat.database.Entities.Organizations;
import com.example.android.neltstat.database.NeltstatDatabase;
import com.example.android.neltstat.netstat.datatype.RawConnectionInfo;
import com.example.android.neltstat.netstat.utils.SystemRuntimeCaller;
import com.example.android.neltstat.netstat.utils.TypeTranslationUtils;
import com.example.android.neltstat.utils.IP;
import com.example.android.neltstat.whois.Whois;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainViewModel extends AndroidViewModel {

    private LiveData<List<ActiveConnections>> activeConnLiveData;
    private LiveData<List<ConnectionsResume>> connResumeLiveData;
    private LiveData<List<Organizations>>     organizationsLiveData;
    private LiveData<List<Applications>>      applicationsLiveData;
    private List<RawConnectionInfo>           rawConnectionInfoList;

    private ExecutorService releasedDataTransferer;
    private ExecutorService appSafeInsertExecutor;
    private ExecutorService connSafeInsertExecutor;
    private ExecutorService orderedExecutor;
    private ExecutorService schedulerService;

    private PriorityQueue<Runnable> pendingTasks;

    private Date last_seen;

    private NeltstatDatabase neltstatDatabase;

    public MainViewModel(Application application) {
        super(application);

        this.neltstatDatabase = NeltstatDatabase.InstanceOf(application);

        this.activeConnLiveData = neltstatDatabase.activeConnectionsDAO().load_all();
        this.connResumeLiveData = neltstatDatabase.connectionsResumeDAO().load_all();
        this.organizationsLiveData = neltstatDatabase.organizationDAO().load_all();
        this.applicationsLiveData  = neltstatDatabase.applicationDAO().load_all();

        releasedDataTransferer = Executors.newCachedThreadPool();
        appSafeInsertExecutor  = Executors.newSingleThreadExecutor();
        connSafeInsertExecutor = Executors.newSingleThreadExecutor();
        orderedExecutor        = Executors.newSingleThreadExecutor();
        schedulerService       = Executors.newSingleThreadExecutor();
        pendingTasks = new PriorityQueue<>(16, runnableComparator);

        update_data();

    }

    public void update_data() {
        schedulerService.execute(new UpdateDataScheduler());
        pendingTasks.add(new ReleasedDataTransferScheduler());
    }

    public LiveData<List<ConnectionsResume>> getConnResumeLiveData() {
        return connResumeLiveData;
    }
    public LiveData<List<ActiveConnections>> getActiveConnLiveData() {
        return activeConnLiveData;
    }
    public LiveData<List<Organizations>> getOrganizationsLiveData() {
        return organizationsLiveData;
    }
    public LiveData<List<Applications>> getApplicationsLiveData() {
        return applicationsLiveData;
    }

    class ReleasedDataTransferScheduler implements Runnable{
        @Override
        public void run() {
            List<ActiveConnections> activeConnectionsList = neltstatDatabase.activeConnectionsDAO().load_released(last_seen);
            List<RawConnectionInfo> rawConnectionInfo_auxList = SystemRuntimeCaller.getAllRawProtocols();
            for (ActiveConnections activeConnections : activeConnectionsList){
                releasedDataTransferer.execute(new SafeReleasedDataTransferer(activeConnections, rawConnectionInfo_auxList));
            }
        }
    }
    class SafeReleasedDataTransferer    implements Runnable{

        private ActiveConnections activeConnections;
        private List<RawConnectionInfo> lastRawInfoList;

        public SafeReleasedDataTransferer(ActiveConnections activeConnections, List<RawConnectionInfo> lastRawInfoList){
            this.activeConnections = activeConnections;
            this.lastRawInfoList = lastRawInfoList;
        }

        @Override
        public void run() {
//            if ( activeConnections.getUid() != 0 )
            for (RawConnectionInfo rawInfo : lastRawInfoList){
                try {
                    if (    activeConnections.getLocal_port()  == TypeTranslationUtils.port_fromHex (rawInfo.getLocalPort()  ) &&
                            activeConnections.getRemote_port() == TypeTranslationUtils.port_fromHex (rawInfo.getRemotePort() ) &&
                            activeConnections.getLocal_ip() .equals(InetAddress.getByAddress(
                                    TypeTranslationUtils.ipBytes_fromString                         (rawInfo.getLocalIP ())) ) &&
                            activeConnections.getRemote_ip().equals(InetAddress.getByAddress(
                                    TypeTranslationUtils.ipBytes_fromString                         (rawInfo.getRemoteIP())) ) &&
                            activeConnections.getProtocol() == rawInfo.getProtocol()
                            ) return;
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
            // Fix pending ids before transfer to resume
            if( activeConnections.getOrg_id() == 0 ){
                Organizations organizations = neltstatDatabase.organizationDAO().load_singleByIP(activeConnections.getRemote_ip());
                if ( organizations != null ) activeConnections.set_orgID(organizations.getOrg_id());
            }
            if ( activeConnections.getApp_id() == 0 ){
                Applications applications = neltstatDatabase.applicationDAO().load_byUID(activeConnections.getUid());
                if ( applications != null ) activeConnections.set_appID(applications.getApp_id());
            }

            Log.d("NELSON", "TRANSFERING \n" +
                    activeConnections.getApp_id()       + "\n" +
                    activeConnections.getOrg_id()       + "\n" +
                    activeConnections.getLocal_port()   + "\n" +
                    activeConnections.getRemote_port()  + "\n" +
                    activeConnections.getProtocol()     + "\n" +
                    activeConnections.getFirst_seen()
            );

            // Transfer
            neltstatDatabase.activeConnectionsDAO().delete(activeConnections);
            neltstatDatabase.connectionsResumeDAO().insert(new ConnectionsResume(activeConnections));
        }
    }
    class UpdateDataScheduler  implements Runnable{
        @Override
        public void run() {
            last_seen = new Date();
            rawConnectionInfoList = SystemRuntimeCaller.getAllRawProtocols();
            for (RawConnectionInfo rawConnectionInfo : rawConnectionInfoList){

                int uid = TypeTranslationUtils.uid(rawConnectionInfo.getUid());
                InetAddress remoteIP = null;
                try {
                    remoteIP = InetAddress.getByAddress(TypeTranslationUtils.ipBytes_fromString(rawConnectionInfo.getRemoteIP()));
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                if(IP.is_whoisSearchable(remoteIP)) pendingTasks.add(new SafeInsertOrgSummary(remoteIP, last_seen));
//                pendingTasks.add(new FixForeignInfo(rawConnectionInfo));
                if(uid!=0) appSafeInsertExecutor.execute(new SafeInsertAppSummary(uid, last_seen));
                connSafeInsertExecutor.execute(new SafeInsertConnection(rawConnectionInfo, last_seen));
            }
            while(!pendingTasks.isEmpty()) orderedExecutor.execute(pendingTasks.poll());
        }
    }
    class SafeInsertConnection implements Runnable{

        private RawConnectionInfo rawConnectionInfo;
        private Date ref_date;

        public SafeInsertConnection(RawConnectionInfo rawConnectionInfo, Date ref_date) {
            this.rawConnectionInfo = rawConnectionInfo;
            this.ref_date = ref_date;
        }

        @Override
        public void run() {
            ActiveConnections activeConnections = null;
            Date yesterday = new Date();
            yesterday.setDate(ref_date.getDate()-1);
            try {
                activeConnections = neltstatDatabase.activeConnectionsDAO().load_unique(
                        InetAddress.getByAddress(TypeTranslationUtils.ipBytes_fromString(rawConnectionInfo.getLocalIP())),
                        InetAddress.getByAddress(TypeTranslationUtils.ipBytes_fromString(rawConnectionInfo.getRemoteIP())),
                        TypeTranslationUtils.port_fromHex(rawConnectionInfo.getLocalPort()),
                        TypeTranslationUtils.port_fromHex(rawConnectionInfo.getRemotePort()),
                        rawConnectionInfo.getProtocol()
                );
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            if (activeConnections == null){
                Log.d("NELSON", "Inserting connection on database");
                try {
                    neltstatDatabase.activeConnectionsDAO().insert(new ActiveConnections(rawConnectionInfo, ref_date));
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
            else  {
                Log.d("NELSON", "Updating connection on database");
                activeConnections.update(rawConnectionInfo, ref_date);
                neltstatDatabase.activeConnectionsDAO().update(activeConnections);
            }
        }

    }
    class SafeInsertAppSummary implements Runnable{

        private int app_uid;
        private Date ref_date;

        public SafeInsertAppSummary(int app_uid, Date ref_date) {
            this.app_uid = app_uid;
            this.ref_date = ref_date;
        }

        @Override
        public void run() {
            Applications applications_fromdb = neltstatDatabase.applicationDAO().load_byUID(app_uid);
            Applications applications_new    = new Applications(AppInfoRetriever.get_appSummary(getApplication(), app_uid), ref_date);
            if (applications_fromdb == null){
                Log.d("NELSON", "Inserting app on database");
                neltstatDatabase.applicationDAO().insert(applications_new);
            }
            else  {
                Log.d("NELSON", "Updating app on database");
                applications_fromdb.update(applications_new);
                neltstatDatabase.applicationDAO().update(applications_fromdb);
            }
        }

    }
    class SafeInsertOrgSummary implements Runnable{

        private InetAddress remote_ip;
        private Date ref_date;

        public SafeInsertOrgSummary(InetAddress remote_ip, Date ref_date) {
            this.remote_ip = remote_ip;
            this.ref_date = ref_date;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void run() {
            Log.d("NELSON", "SafeInsert of "+remote_ip.getHostAddress());
            Organizations organizations = neltstatDatabase
                    .organizationDAO()
                    .load_singleByIP(remote_ip);
            if (organizations == null) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Whois whois = new Whois(remote_ip.getHostAddress()).run();
                neltstatDatabase.organizationDAO().insert(new Organizations(whois.getSummary(), ref_date));
            }
        }

    }
    class FixForeignInfo       implements Runnable{

        private RawConnectionInfo rawConnectionInfo;

        public FixForeignInfo(RawConnectionInfo rawConnectionInfo){
            this.rawConnectionInfo = rawConnectionInfo;
        }

        @Override
        public void run() {
            Log.d("NELSON", "Trying to fix connection of local port "+TypeTranslationUtils.port_fromHex(rawConnectionInfo.getLocalPort()) );
            ActiveConnections activeConnections = null;
            Date ref_date = new Date();
            ref_date.setDate(new Date().getDay()-1);
            try {
                activeConnections = neltstatDatabase.activeConnectionsDAO().load_unique(
                        InetAddress.getByAddress(TypeTranslationUtils.ipBytes_fromString(rawConnectionInfo.getLocalIP())),
                        InetAddress.getByAddress(TypeTranslationUtils.ipBytes_fromString(rawConnectionInfo.getRemoteIP())),
                        TypeTranslationUtils.port_fromHex(rawConnectionInfo.getLocalPort()),
                        TypeTranslationUtils.port_fromHex(rawConnectionInfo.getRemotePort()),
                        TypeTranslationUtils.uid(rawConnectionInfo.getUid()),
                        rawConnectionInfo.getProtocol(),
                        ref_date
                );
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            if (activeConnections == null){
                Log.d("NELSON", "ActiveConnection not found on database. Cannot fix its foreign information.");
                return;
            }
            else  {
                Log.d("NELSON", "ActiveConnection found on database. Fixing its foreign information");
                if( activeConnections.getOrg_id() == 0 ){
                    Organizations organizations = neltstatDatabase.organizationDAO().load_singleByIP(activeConnections.getRemote_ip());
                    if ( organizations != null ) activeConnections.set_orgID(organizations.getOrg_id());
                }
                if ( activeConnections.getApp_id() == 0 ){
                    Applications applications = neltstatDatabase.applicationDAO().load_byUID(activeConnections.getUid());
                    if ( applications != null ) activeConnections.set_appID(applications.getApp_id());
                }

                neltstatDatabase.activeConnectionsDAO().update(activeConnections);
            }
        }
    }

    private Comparator<Runnable> runnableComparator = new Comparator<Runnable>() {
        @Override
        public int compare(Runnable o1, Runnable o2) {

            if (o1 instanceof SafeInsertOrgSummary && o2 instanceof FixForeignInfo) return -1;
            if (o2 instanceof SafeInsertOrgSummary && o1 instanceof FixForeignInfo) return  1;

            if (o1 instanceof SafeInsertOrgSummary && o2 instanceof ReleasedDataTransferScheduler) return -1;
            if (o2 instanceof SafeInsertOrgSummary && o1 instanceof ReleasedDataTransferScheduler) return  1;

            if (o1 instanceof FixForeignInfo && o2 instanceof ReleasedDataTransferScheduler) return -1;
            if (o2 instanceof FixForeignInfo && o1 instanceof ReleasedDataTransferScheduler) return  1;

            return 0;
        }
    };

    @Override
    protected void onCleared() {
        neltstatDatabase.destroy();
        super.onCleared();
    }

}
