package com.example.android.neltstat.database.Entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.android.neltstat.netstat.Intents.PROTOCOL;
import com.example.android.neltstat.netstat.Intents.STATE_CODE;
import com.example.android.neltstat.netstat.datatype.RawConnectionInfo;
import com.example.android.neltstat.netstat.utils.TypeTranslationUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

@Entity(
        tableName = "active_connections",
        indices = @Index(
                value = {"local_ip", "remote_ip", "local_port", "remote_port", "protocol", "first_seen", "uid"},
                unique = true)
//        foreignKeys = {
//            @ForeignKey(
//                    entity=Organizations.class, parentColumns = "org_id", childColumns = "org_id",
//                    onUpdate = ForeignKey.CASCADE, onDelete = ForeignKey.CASCADE),
//            @ForeignKey(
//                    entity=Applications.class,  parentColumns = "app_id", childColumns = "app_id",
//                    onUpdate = ForeignKey.CASCADE, onDelete = ForeignKey.CASCADE)
//        }
)
public class ActiveConnections {

    @NonNull
    @PrimaryKey(autoGenerate = true )
    private int id;

    private int app_id;
    private int org_id;

    private InetAddress local_ip;
    private InetAddress remote_ip;
    private int local_port;
    private int remote_port;
    private int protocol;

    private int uid;

    private Date established;
    private Date sync_sent;
    private Date sync_received;
    private Date fin_wait1;
    private Date fin_wait2;
    private Date time_wait;
    private Date closed;
    private Date closed_wait;
    private Date last_ack;
    private Date listening;
    private Date closing;
    private Date max_end;

    private Date first_seen;
    private Date last_seen;

    public ActiveConnections(@NonNull int id, int app_id, int org_id, InetAddress local_ip, InetAddress remote_ip, int local_port, int remote_port, int protocol, int uid, Date established, Date sync_sent, Date sync_received, Date fin_wait1, Date fin_wait2, Date time_wait, Date closed, Date closed_wait, Date last_ack, Date listening, Date closing, Date max_end, Date first_seen, Date last_seen) {
        this.id = id;
        this.app_id = app_id;
        this.org_id = org_id;
        this.local_ip = local_ip;
        this.remote_ip = remote_ip;
        this.local_port = local_port;
        this.remote_port = remote_port;
        this.protocol = protocol;
        this.uid = uid;
        this.established = established;
        this.sync_sent = sync_sent;
        this.sync_received = sync_received;
        this.fin_wait1 = fin_wait1;
        this.fin_wait2 = fin_wait2;
        this.time_wait = time_wait;
        this.closed = closed;
        this.closed_wait = closed_wait;
        this.last_ack = last_ack;
        this.listening = listening;
        this.closing = closing;
        this.max_end = max_end;
        this.first_seen = first_seen;
        this.last_seen = last_seen;
    }

    @Ignore
    public ActiveConnections(RawConnectionInfo rawConnectionInfo, Date ref_date) throws UnknownHostException {
        local_ip = InetAddress.getByAddress(
                TypeTranslationUtils.ipBytes_fromString(
                        rawConnectionInfo.getLocalIP()));
        remote_ip = InetAddress.getByAddress(
                TypeTranslationUtils.ipBytes_fromString(
                        rawConnectionInfo.getRemoteIP()));
        local_port  = TypeTranslationUtils.port_fromHex(rawConnectionInfo.getLocalPort());
        remote_port = TypeTranslationUtils.port_fromHex(rawConnectionInfo.getRemotePort());
        uid         = TypeTranslationUtils.uid(rawConnectionInfo.getUid());
        set_protocol(rawConnectionInfo);
        clear_states();
        set_state(rawConnectionInfo, ref_date);
        first_seen  = ref_date;
        last_seen   = ref_date;
    }
    @Ignore
    private ActiveConnections set_protocol(RawConnectionInfo rawInfo){
        if      (rawInfo.getProtocol() == PROTOCOL.TCP4) protocol = PROTOCOL.TCP4;
        else if (rawInfo.getProtocol() == PROTOCOL.TCP6) protocol = PROTOCOL.TCP6;
        else if (rawInfo.getProtocol() == PROTOCOL.UDP4) protocol = PROTOCOL.UDP4;
        else if (rawInfo.getProtocol() == PROTOCOL.UDP6) protocol = PROTOCOL.UDP6;
        else                                             protocol = PROTOCOL.NULL;
        return this;
    }
    @Ignore
    private ActiveConnections set_state(RawConnectionInfo rawInfo, Date ref_date){
        switch (TypeTranslationUtils.stateCode(rawInfo.getStateCode())){
            case STATE_CODE.TCP_ESTABLISHED :
                established = ref_date;
                break;
            case STATE_CODE.TCP_SYN_SENT    :
                sync_sent = ref_date;
                break;
            case STATE_CODE.TCP_SYN_RECV    :
                sync_received = ref_date;
                break;
            case STATE_CODE.TCP_FIN_WAIT1   :
                fin_wait1 = ref_date;
                break;
            case STATE_CODE.TCP_FIN_WAIT2   :
                fin_wait2 = ref_date;
                break;
            case STATE_CODE.TCP_TIME_WAIT   :
                time_wait = ref_date;
                break;
            case STATE_CODE.TCP_CLOSE       :
                closed = ref_date;
                break;
            case STATE_CODE.TCP_CLOSE_WAIT  :
                closed_wait = ref_date;
                break;
            case STATE_CODE.TCP_LAST_ACK    :
                last_ack = ref_date;
                break;
            case STATE_CODE.TCP_LISTEN      :
                listening = ref_date;
                break;
            case STATE_CODE.TCP_CLOSING     :
                closing = ref_date;
                break;
            case STATE_CODE.TCP_MAX_END     :
                max_end = ref_date;
                break;
        }
        return this;
    }
    @Ignore
    private void clear_states(){
        this.established   = new Date(0);
        this.sync_sent     = new Date(0);
        this.sync_received = new Date(0);
        this.fin_wait1     = new Date(0);
        this.fin_wait2     = new Date(0);
        this.time_wait     = new Date(0);
        this.closed        = new Date(0);
        this.closed_wait   = new Date(0);
        this.last_ack      = new Date(0);
        this.listening     = new Date(0);
        this.closing       = new Date(0);
        this.max_end       = new Date(0);
    }
    @Ignore
    public ActiveConnections set_orgID(int id){
        org_id = id;
        return this;
    }
    @Ignore
    public ActiveConnections set_appID(int id){
        app_id = id;
        return this;
    }
    @Ignore
    public ActiveConnections update(RawConnectionInfo rawConnectionInfo, Date ref_date) {
//        local_ip = InetAddress.getByAddress(
//                TypeTranslationUtils.ipBytes_fromString(
//                        rawConnectionInfo.getLocalIP()));
//        remote_ip = InetAddress.getByAddress(
//                TypeTranslationUtils.ipBytes_fromString(
//                        rawConnectionInfo.getRemoteIP()));
//        local_port = TypeTranslationUtils.port_fromHex(rawConnectionInfo.getLocalPort());
//        remote_port = TypeTranslationUtils.port_fromHex(rawConnectionInfo.getRemotePort());
//        uid         = rawConnectionInfo.getUid();
//        set_protocol(rawConnectionInfo);
        int new_uid = TypeTranslationUtils.uid(rawConnectionInfo.getUid());
        if ( this.uid==0 && new_uid!=0) uid = new_uid;
        set_state(rawConnectionInfo, ref_date);
        last_seen = ref_date;
        return this;
    }

    @Ignore
    public Boolean did_stage_4(){
        if(     this.closed     .equals(new Date(0))    &&
                this.time_wait  .equals(new Date(0))    ) return false;
        return true;
    }
    @Ignore
    public Boolean did_stage_3(){
        if(     this.fin_wait1   .equals(new Date(0))   &&
                this.fin_wait2   .equals(new Date(0))   &&
                this.closed_wait .equals(new Date(0))   &&
                this.last_ack    .equals(new Date(0))   ) return false;
        return true;
    }
    @Ignore
    public Boolean did_stage_2(){
        if(     this.established.equals(new Date(0))    &&
                this.listening  .equals(new Date(0))    ) return false;
        return true;
    }
    @Ignore
    public Boolean did_stage_1(){
        if(     this.sync_sent    .equals(new Date(0))  &&
                this.sync_received.equals(new Date(0))  ) return false;
        return true;
    }
    @Ignore
    public Boolean did_stage_0(){
        if(     this.max_end.equals(new Date(0))    &&
                this.closing.equals(new Date(0))    ) return false;
        return true;
    }


    @NonNull
    public int getId() {
        return id;
    }
    public int getApp_id() {
        return app_id;
    }
    public int getOrg_id() {
        return org_id;
    }

    public InetAddress getLocal_ip() {
        return local_ip;
    }
    public InetAddress getRemote_ip() {
        return remote_ip;
    }
    public int getLocal_port() {
        return local_port;
    }
    public int getRemote_port() {
        return remote_port;
    }
    public int getProtocol() {
        return protocol;
    }

    public int getUid(){
        return uid;
    }

    public Date getEstablished() {
        return established;
    }
    public Date getSync_sent() {
        return sync_sent;
    }
    public Date getSync_received() {
        return sync_received;
    }
    public Date getFin_wait1() {
        return fin_wait1;
    }
    public Date getFin_wait2() {
        return fin_wait2;
    }
    public Date getTime_wait() {
        return time_wait;
    }
    public Date getClosed() {
        return closed;
    }
    public Date getClosed_wait() {
        return closed_wait;
    }
    public Date getLast_ack() {
        return last_ack;
    }
    public Date getListening() {
        return listening;
    }
    public Date getClosing() {
        return closing;
    }
    public Date getMax_end() {
        return max_end;
    }

    public Date getFirst_seen() {
        return first_seen;
    }
    public Date getLast_seen() {
        return last_seen;
    }

}
