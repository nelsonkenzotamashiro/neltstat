package com.example.android.neltstat.RecyclerViewAdapters;


import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.neltstat.R;
import com.example.android.neltstat.database.Entities.Applications;

import java.text.SimpleDateFormat;
import java.util.List;

public class ApplicationsRecyclerViewAdapter
        extends RecyclerView.Adapter<ApplicationsRecyclerViewAdapter.AppInfoViewHolder> {

    private Context            context;
    private List<Applications> applicationsArrayList;

    public void setApplicationsArrayList(List<Applications> applicationsArrayList) {
        if (applicationsArrayList == null) return;
        this.applicationsArrayList = applicationsArrayList;
    }

    public ApplicationsRecyclerViewAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public AppInfoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.appinfo_summary,viewGroup, false);
        AppInfoViewHolder appInfoViewHolder = new AppInfoViewHolder(view);
        return appInfoViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AppInfoViewHolder appInfoViewHolder, int i) {
        appInfoViewHolder.set_properties(applicationsArrayList.get(i));
    }

    @Override
    public int getItemCount() {
        if (applicationsArrayList == null) return 0;
        return applicationsArrayList.size();
    }

    class AppInfoViewHolder extends RecyclerView.ViewHolder{

        TextView appName_TextView,
                version_TextView,
                lastSeen_TextView;

        ImageView app_icon;

        public AppInfoViewHolder(@NonNull View itemView) {
            super(itemView);
            appName_TextView     = itemView.findViewById(R.id.app_name      );
            version_TextView     = itemView.findViewById(R.id.version       );
            lastSeen_TextView    = itemView.findViewById(R.id.duration);
            app_icon             = itemView.findViewById(R.id.app_icon      );
        }

        public void set_properties(Applications applications){
            appName_TextView    .setText(applications.getApp_name());
            version_TextView    .setText(applications.getVersion());
            lastSeen_TextView   .setText(new SimpleDateFormat("yyyy-MM-dd").format(applications.getLast_seen()));
            try {
                app_icon.setImageDrawable(context.getPackageManager().getApplicationIcon(applications.getApp_fullname()));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

}
