package com.example.android.neltstat.RecyclerViewAdapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.android.neltstat.R;
import com.example.android.neltstat.database.Entities.ConnectionsResume;
import com.example.android.neltstat.netstat.Intents.PROTOCOL;

import java.text.SimpleDateFormat;
import java.util.List;

public class ConnectionResumeRecyclerViewAdapter extends RecyclerView.Adapter<ConnectionResumeRecyclerViewAdapter.ConnectionResumeViewHolder> {

    private List<ConnectionsResume> connectionsResumeList;

    @NonNull
    @Override
    public ConnectionResumeRecyclerViewAdapter.ConnectionResumeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.connection_resume_raw, viewGroup, false);
        ConnectionResumeViewHolder connectionViewHolder = new ConnectionResumeViewHolder(view);
        return connectionViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ConnectionResumeRecyclerViewAdapter.ConnectionResumeViewHolder connectionViewHolder, int i) {
        connectionViewHolder.set(connectionsResumeList.get(i));
    }

    @Override
    public int getItemCount() {
        if(connectionsResumeList ==null) return 0;
        return connectionsResumeList.size();
    }

    public void setConnectionArrayList(List<ConnectionsResume> connectionsResumeList) {
        this.connectionsResumeList = connectionsResumeList;
        notifyDataSetChanged();
    }

    class ConnectionResumeViewHolder extends RecyclerView.ViewHolder{

        private TextView
                appID_textView,
                orgID_textView,
                connInfo_textView,
                protocol_textView,

                date_established_textView,
                date_sync_sent_textView,
                date_sync_rec_textView,
                date_fin_wait1_textView,
                date_fin_wait2_textView,
                date_time_wait_textView,
                date_closed_textView,
                date_closed_wait_textView,
                date_last_ack_textView,
                date_listening_textView,
                date_closing_textView,
                date_max_end_textView
        ;

        private RelativeLayout
                row_established,
                row_sync_sent,
                row_sync_rec,
                row_fin_wait1,
                row_fin_wait2,
                row_time_wait,
                row_closed,
                row_closed_wait,
                row_last_ack,
                row_listening,
                row_closing,
                row_max_end
        ;

        private SimpleDateFormat dateFormat;

        public ConnectionResumeViewHolder(@NonNull View itemView) {
            super(itemView);
            appID_textView     = itemView.findViewById(R.id.app_id       );
            orgID_textView     = itemView.findViewById(R.id.org_id       );
            connInfo_textView  = itemView.findViewById(R.id.port_ip_info );
            protocol_textView  = itemView.findViewById(R.id.protocol     );

            row_established = itemView.findViewById(R.id.row_state_established ); date_established_textView = itemView.findViewById(R.id.state_date_established );
            row_sync_sent   = itemView.findViewById(R.id.row_state_sync_sent   ); date_sync_sent_textView   = itemView.findViewById(R.id.state_date_sync_sent   );
            row_sync_rec    = itemView.findViewById(R.id.row_state_sync_rec    ); date_sync_rec_textView    = itemView.findViewById(R.id.state_date_sync_rec    );
            row_fin_wait1   = itemView.findViewById(R.id.row_state_fin_wait1   ); date_fin_wait1_textView   = itemView.findViewById(R.id.state_date_fin_wait1   );
            row_fin_wait2   = itemView.findViewById(R.id.row_state_fin_wait2   ); date_fin_wait2_textView   = itemView.findViewById(R.id.state_date_fin_wait2   );
            row_time_wait   = itemView.findViewById(R.id.row_state_time_wait   ); date_time_wait_textView   = itemView.findViewById(R.id.state_date_time_wait   );
            row_closed      = itemView.findViewById(R.id.row_state_closed      ); date_closed_textView      = itemView.findViewById(R.id.state_date_closed      );
            row_closed_wait = itemView.findViewById(R.id.row_state_closed_wait ); date_closed_wait_textView = itemView.findViewById(R.id.state_date_closed_wait );
            row_last_ack    = itemView.findViewById(R.id.row_state_last_ack    ); date_last_ack_textView    = itemView.findViewById(R.id.state_date_last_ack    );
            row_listening   = itemView.findViewById(R.id.row_state_listening   ); date_listening_textView   = itemView.findViewById(R.id.state_date_listening   );
            row_closing     = itemView.findViewById(R.id.row_state_closing     ); date_closing_textView     = itemView.findViewById(R.id.state_date_closing     );
            row_max_end     = itemView.findViewById(R.id.row_state_max_end     ); date_max_end_textView     = itemView.findViewById(R.id.state_date_max_end     );
            dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }

        public void set(ConnectionsResume connectionsResume){
            appID_textView     .setText(""+connectionsResume.getApp_id());
            orgID_textView     .setText(""+connectionsResume.getOrg_id());
            connInfo_textView  .setText(
                    connectionsResume.getLocal_port()                   + ":" +
                    connectionsResume.getRemote_ip().getHostAddress()   + ":" +
                    connectionsResume.getRemote_port()
            );
            protocol_textView  .setText(PROTOCOL.NAME[connectionsResume.getProtocol()]);

            if(connectionsResume.did_established() ) { row_established.setVisibility(View.VISIBLE);date_established_textView.setText(dateFormat.format(connectionsResume.getEstablished() )); } else row_established.setVisibility(View.GONE);
            if(connectionsResume.did_sync_sent()   ) { row_sync_sent  .setVisibility(View.VISIBLE);date_sync_sent_textView  .setText(dateFormat.format(connectionsResume.getSync_sent()   )); } else row_sync_sent  .setVisibility(View.GONE);
            if(connectionsResume.did_sync_rec()    ) { row_sync_rec   .setVisibility(View.VISIBLE);date_sync_rec_textView   .setText(dateFormat.format(connectionsResume.getSync_rec()    )); } else row_sync_rec   .setVisibility(View.GONE);
            if(connectionsResume.did_fin_wait1()   ) { row_fin_wait1  .setVisibility(View.VISIBLE);date_fin_wait1_textView  .setText(dateFormat.format(connectionsResume.getFin_wait1()   )); } else row_fin_wait1  .setVisibility(View.GONE);
            if(connectionsResume.did_fin_wait2()   ) { row_fin_wait2  .setVisibility(View.VISIBLE);date_fin_wait2_textView  .setText(dateFormat.format(connectionsResume.getFin_wait2()   )); } else row_fin_wait2  .setVisibility(View.GONE);
            if(connectionsResume.did_time_wait()   ) { row_time_wait  .setVisibility(View.VISIBLE);date_time_wait_textView  .setText(dateFormat.format(connectionsResume.getTime_wait()   )); } else row_time_wait  .setVisibility(View.GONE);
            if(connectionsResume.did_closed()      ) { row_closed     .setVisibility(View.VISIBLE);date_closed_textView     .setText(dateFormat.format(connectionsResume.getClosed()      )); } else row_closed     .setVisibility(View.GONE);
            if(connectionsResume.did_closed_wait() ) { row_closed_wait.setVisibility(View.VISIBLE);date_closed_wait_textView.setText(dateFormat.format(connectionsResume.getClosed_wait() )); } else row_closed_wait.setVisibility(View.GONE);
            if(connectionsResume.did_last_ack()    ) { row_last_ack   .setVisibility(View.VISIBLE);date_last_ack_textView   .setText(dateFormat.format(connectionsResume.getLast_ack()    )); } else row_last_ack   .setVisibility(View.GONE);
            if(connectionsResume.did_listening()   ) { row_listening  .setVisibility(View.VISIBLE);date_listening_textView  .setText(dateFormat.format(connectionsResume.getListening()   )); } else row_listening  .setVisibility(View.GONE);
            if(connectionsResume.did_closing()     ) { row_closing    .setVisibility(View.VISIBLE);date_closing_textView    .setText(dateFormat.format(connectionsResume.getClosing()     )); } else row_closing    .setVisibility(View.GONE);
            if(connectionsResume.did_max_end()     ) { row_max_end    .setVisibility(View.VISIBLE);date_max_end_textView    .setText(dateFormat.format(connectionsResume.getMax_end()     )); } else row_max_end    .setVisibility(View.GONE);

        }

//        @Override
//        public void onClick(View v) {
//            connectionsClickInterface.onClick(activeConnectionsArrayList.get(getAdapterPosition()).getRemote_ip().getHostAddress());
//        }

    }


}
