package com.example.android.neltstat.database.Entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.net.InetAddress;
import java.util.Date;

@Entity(
        tableName = "connections_resume",
        indices = @Index(
                value = {"app_id", "org_id", "local_port", "remote_port", "protocol", "first_seen"},
                unique = true)
)
public class ConnectionsResume {

    @NonNull
    @PrimaryKey(autoGenerate = true )
    private int id;

    private int app_id;
    private int org_id;

    private int local_port;
    private InetAddress remote_ip;
    private int remote_port;
    private int protocol;

    private Date established;
    private Date sync_sent;
    private Date sync_rec;
    private Date fin_wait1;
    private Date fin_wait2;
    private Date time_wait;
    private Date closed;
    private Date closed_wait;
    private Date last_ack;
    private Date listening;
    private Date closing;
    private Date max_end;

    private Date first_seen;
    private Date last_seen;

    public ConnectionsResume(@NonNull int id, int app_id, int org_id, int local_port, InetAddress remote_ip, int remote_port, int protocol, Date established, Date sync_sent, Date sync_rec, Date fin_wait1, Date fin_wait2, Date time_wait, Date closed, Date closed_wait, Date last_ack, Date listening, Date closing, Date max_end, Date first_seen, Date last_seen) {
        this.id = id;
        this.app_id = app_id;
        this.org_id = org_id;
        this.local_port = local_port;
        this.remote_ip = remote_ip;
        this.remote_port = remote_port;
        this.protocol = protocol;
        this.established = established;
        this.sync_sent = sync_sent;
        this.sync_rec = sync_rec;
        this.fin_wait1 = fin_wait1;
        this.fin_wait2 = fin_wait2;
        this.time_wait = time_wait;
        this.closed = closed;
        this.closed_wait = closed_wait;
        this.last_ack = last_ack;
        this.listening = listening;
        this.closing = closing;
        this.max_end = max_end;
        this.first_seen = first_seen;
        this.last_seen = last_seen;
    }

    @Ignore
    public ConnectionsResume(ActiveConnections activeConnections){
        app_id = activeConnections.getApp_id();
        org_id = activeConnections.getOrg_id();

        local_port  = activeConnections.getLocal_port ();
        remote_port = activeConnections.getRemote_port();
        remote_ip   = activeConnections.getRemote_ip();

        protocol = activeConnections.getProtocol();

        established   = activeConnections.getEstablished();
        sync_sent     = activeConnections.getSync_sent();
        sync_rec = activeConnections.getSync_received();
        fin_wait1     = activeConnections.getFin_wait1();
        fin_wait2     = activeConnections.getFin_wait2();
        time_wait     = activeConnections.getTime_wait();
        closed        = activeConnections.getClosed();
        closed_wait   = activeConnections.getClosed_wait();
        last_ack      = activeConnections.getLast_ack();
        listening     = activeConnections.getListening();
        closing       = activeConnections.getClosing();
        max_end       = activeConnections.getMax_end();

        first_seen = activeConnections.getFirst_seen();
        last_seen  = activeConnections.getLast_seen();

    }

    @Ignore
    public Boolean did_established(){
        if(established.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_sync_sent(){
        if(sync_sent.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_sync_rec(){
        if(sync_rec.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_fin_wait1(){
        if(fin_wait1.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_fin_wait2(){
        if(fin_wait2.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_time_wait(){
        if(time_wait.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_closed(){
        if(closed.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_closed_wait(){
        if(closed_wait.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_last_ack(){
        if(last_ack.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_listening(){
        if(listening.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_closing(){
        if(closing.equals(new Date(0))) return false;
        return true;
    }
    @Ignore
    public Boolean did_max_end(){
        if(max_end.equals(new Date(0))) return false;
        return true;
    }

    @NonNull
    public int getId() {
        return id;
    }
    public int getApp_id() {
        return app_id;
    }
    public int getOrg_id() {
        return org_id;
    }
    public int getLocal_port() {
        return local_port;
    }
    public int getRemote_port() {
        return remote_port;
    }
    public int getProtocol() {
        return protocol;
    }
    public InetAddress getRemote_ip() {
        return remote_ip;
    }
    public Date getEstablished() {
        return established;
    }
    public Date getSync_sent() {
        return sync_sent;
    }
    public Date getSync_rec() {
        return sync_rec;
    }
    public Date getFin_wait1() {
        return fin_wait1;
    }
    public Date getFin_wait2() {
        return fin_wait2;
    }
    public Date getTime_wait() {
        return time_wait;
    }
    public Date getClosed() {
        return closed;
    }
    public Date getClosed_wait() {
        return closed_wait;
    }
    public Date getLast_ack() {
        return last_ack;
    }
    public Date getListening() {
        return listening;
    }
    public Date getClosing() {
        return closing;
    }
    public Date getMax_end() {
        return max_end;
    }
    public Date getFirst_seen() {
        return first_seen;
    }
    public Date getLast_seen() {
        return last_seen;
    }

}
