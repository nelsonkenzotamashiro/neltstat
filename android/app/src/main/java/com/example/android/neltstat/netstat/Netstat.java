package com.example.android.neltstat.netstat;

import android.content.Context;

import com.example.android.neltstat.netstat.Intents.PROTOCOL;
import com.example.android.neltstat.netstat.datatype.Connection;
import com.example.android.neltstat.netstat.utils.ConnectionModifier;
import com.example.android.neltstat.netstat.utils.SystemRuntimeCaller;

import java.util.ArrayList;

/**
 *  Netstat is a Manager to handle ActiveConnections
 *      - RecyclerView.Adapter may request the list directly through getConnectionList and params
 *
 * */
public class Netstat {

    private static Netstat instance;
    private Context context;

    //  TODO (017): Refactor so that the class provides DetailedConnections rather than ActiveConnections
    //      - DetailedConnections must contain
    //      -
    private ArrayList<Connection> tcp4_connections;
    private ArrayList<Connection> tcp6_connections;
    private ArrayList<Connection> udp4_connections;
    private ArrayList<Connection> udp6_connections;

    private Netstat(Context context) {
        this.context          = context;
        this.tcp4_connections = new ArrayList<>();
        this.tcp6_connections = new ArrayList<>();
        this.udp4_connections = new ArrayList<>();
        this.udp6_connections = new ArrayList<>();
    }

    public static Netstat instanceOf(Context context){
        if (instance == null) instance = new Netstat(context);
        return instance;
    }

    public void set_AllProtocols(){
        set(PROTOCOL.TCP4);
        set(PROTOCOL.TCP6);
        set(PROTOCOL.UDP4);
        set(PROTOCOL.UDP6);
    }

    public void set(int protocol){

//        ArrayList<Connection> connectionArrayList = null;
        switch (protocol){
            case PROTOCOL.TCP4:
//                connectionArrayList = tcp4_connections;
                tcp4_connections = SystemRuntimeCaller.getRichConnections(protocol);
                ConnectionModifier.set_AppInfo(context, tcp4_connections);
                ConnectionModifier.set_OrgInfo(context, tcp4_connections); // to be implemented
                break;
            case PROTOCOL.TCP6:
//                connectionArrayList = tcp6_connections;
                tcp6_connections = SystemRuntimeCaller.getRichConnections(protocol);
                ConnectionModifier.set_AppInfo(context, tcp6_connections);
                ConnectionModifier.set_OrgInfo(context, tcp6_connections); // to be implemented
                break;
            case PROTOCOL.UDP4:
//                connectionArrayList = udp4_connections;
                udp4_connections = SystemRuntimeCaller.getRichConnections(protocol);
                ConnectionModifier.set_AppInfo(context, udp4_connections);
                ConnectionModifier.set_OrgInfo(context, udp4_connections); // to be implemented
                break;
            case PROTOCOL.UDP6:
//                connectionArrayList = udp6_connections;
                udp6_connections = SystemRuntimeCaller.getRichConnections(protocol);
                ConnectionModifier.set_AppInfo(context, udp6_connections);
                ConnectionModifier.set_OrgInfo(context, udp6_connections); // to be implemented
                break;
        }
//        Log.d("NELSON", "active_connections="+connectionArrayList.size());

    }

//    BUG: did not work for binary declaration, but worked with integers
    public final static int MODE_ALL        = 15; // 0b1111;
    public final static int MODE_TCP_ONLY   =  3; // 0b0011;
    public final static int MODE_UDP_ONLY   = 12; // 0b1100;
    public final static int MODE_TCP4       =  1; // 0b0001;
    public final static int MODE_TCP6       =  2; // 0b0010;
    public final static int MODE_UDP4       =  4; // 0b0100;
    public final static int MODE_UDP6       =  8; // 0b1000;

    public ArrayList<Connection> get_connectionList(int mode){
        ArrayList<Connection> connections = new ArrayList<>();
        int aux_mode = mode & MODE_ALL;
        if( (aux_mode & MODE_TCP4) > 0 ) connections.addAll(tcp4_connections);
        if( (aux_mode & MODE_TCP6) > 0 ) connections.addAll(tcp6_connections);
        if( (aux_mode & MODE_UDP4) > 0 ) connections.addAll(udp4_connections);
        if( (aux_mode & MODE_UDP6) > 0 ) connections.addAll(udp6_connections);
        //        Log.d("NELSON", "tcp4  active_connections="+tcp4_connections.size());
//        Log.d("NELSON", "tcp6  active_connections="+tcp6_connections.size());
//        Log.d("NELSON", "udp4  active_connections="+udp4_connections.size());
//        Log.d("NELSON", "udp6  active_connections="+udp6_connections.size());
//        Log.d("NELSON", "total active_connections="+active_connections.size());
        return connections;
    }

    public void destroy(){

        tcp4_connections.clear();
        tcp4_connections = null;

        tcp6_connections.clear();
        tcp6_connections = null;

        udp4_connections.clear();
        udp4_connections = null;

        udp6_connections.clear();
        udp6_connections = null;

        instance = null;

    }

    public void reset(){
        tcp4_connections = new ArrayList<>();
        tcp6_connections = new ArrayList<>();
        udp4_connections = new ArrayList<>();
        udp6_connections = new ArrayList<>();
    }

}
