package com.example.android.neltstat.database.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.android.neltstat.database.Entities.ActiveConnections;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;

@Dao
public interface ActiveConnectionsDAO {

    @Insert
    void insert(ActiveConnections...connections);
    @Insert
    void insert(List<ActiveConnections> connections);

    @Update
    void update(ActiveConnections...connections);
    @Update
    void update(List<ActiveConnections> connections);

    @Delete
    void delete(ActiveConnections...connections);
    @Delete
    void delete(List<ActiveConnections> connections);

    @Query("select * from active_connections order by last_seen desc, first_seen desc;")
    LiveData<List<ActiveConnections>> load_all();
    @Query("select * from active_connections where last_seen < :ref_date order by last_seen desc, first_seen desc;")
    List<ActiveConnections> load_released(Date ref_date);
    @Query("select * from active_connections where first_seen > :min_date and first_seen < :max_date;")
    LiveData<List<ActiveConnections>> load_byDateInterval(Date min_date, Date max_date);
    @Query("select * from active_connections where org_id = :org_id;")
    List<ActiveConnections> load_byOrgID(int org_id);
    @Query("select * from active_connections where app_id = :app_id;")
    List<ActiveConnections> load_byAppID(int app_id);
    @Query("SELECT * FROM active_connections " +
            "WHERE local_ip = :local_ip " +
            "AND remote_ip = :remote_ip " +
            "AND local_port = :local_port " +
            "AND remote_port= :remote_port " +
            "AND protocol = :protocol " +
            "AND uid = :uid " +
            "AND first_seen > :ref_date " +
            "LIMIT 1;")
    ActiveConnections load_unique(InetAddress local_ip,
                                  InetAddress remote_ip,
                                  int local_port,
                                  int remote_port,
                                  int uid,
                                  int protocol,
                                  Date ref_date
    );
    @Query("SELECT * FROM active_connections " +
            "WHERE local_ip = :local_ip " +
            "AND remote_ip = :remote_ip " +
            "AND local_port = :local_port " +
            "AND remote_port= :remote_port " +
            "AND protocol = :protocol " +
            "LIMIT 1;")
    ActiveConnections load_unique(InetAddress local_ip,
                                  InetAddress remote_ip,
                                  int local_port,
                                  int remote_port,
                                  int protocol
    );

}
