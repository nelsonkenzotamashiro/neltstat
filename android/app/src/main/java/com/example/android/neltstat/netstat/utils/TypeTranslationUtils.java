package com.example.android.neltstat.netstat.utils;

import android.util.Log;

/***
 *  TypeTranslationUtils is a static WhoisInterpreter of raw coded data
 */
public class TypeTranslationUtils {

    public static byte[] ipBytes_fromString(String ip){
        return reorder( toBytes(ip) );
    }

    private static byte[] reorder(byte[] ip){
        byte[] new_ip = new byte[ip.length];
        for(int i=0; i<ip.length; i+=4){
            new_ip[i  ] = ip[i+3];
            new_ip[i+1] = ip[i+2];
            new_ip[i+2] = ip[i+1];
            new_ip[i+3] = ip[i  ];
        }
        return new_ip;
    }

    private static byte[] toBytes(String ip){
        int size = ip.length()/2;
        byte[] bytes = new byte[size];
        for (int i=0; i<size; i++){
            bytes[i] = (byte)Integer.parseInt(ip.substring(i*2,(i+1)*2),16);
        }
        return bytes;
    }

    public static int port_fromHex(String port){
        return Integer.parseInt(port, 16);
    }

    public static int stateCode(String state){
        return Integer.parseInt(state, 16);
    }

    public static int uid(String uid){
        return Integer.parseInt(uid);
    }

}
