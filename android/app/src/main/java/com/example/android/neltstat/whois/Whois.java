package com.example.android.neltstat.whois;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.example.android.neltstat.whois.Intents.HOSTS;
import com.example.android.neltstat.whois.Utils.WSocket;
import com.example.android.neltstat.whois.datatype.OrgSummary;
import com.example.android.neltstat.whois.datatype.Response;

import java.io.IOException;

/**
 *  Class that controls WSocket and Response in order to follow referer
 *      and provide precise response.
 *      - provides access to the last whois query
 *      - provides access to essential information
 */
public class Whois {

    private OrgSummary      orgSummary;
    private Response        lastResponse;
    private String          targetHost;
    private String          lastReferer = HOSTS.IANA;
    private int             max_queries = 2;

    public Whois(String targetHost) {
        this.targetHost = targetHost;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Whois run() {
        lastResponse = query(lastReferer, targetHost);
        orgSummary = lastResponse.get_summary();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            lastResponse.log_summary();
        }
        while (nextState()!=null){
            lastReferer = nextState();
            lastResponse = query(lastReferer, targetHost);
            orgSummary.update(lastResponse.get_summary());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                lastResponse.log_summary();
            }
            if (max_queries--==0) break;
        }
        return this;
    }

    // LOG METHODS
    public OrgSummary getSummary() {
        return orgSummary;
    }

    public Response getLastResponse() {
        return lastResponse;
    }

    // FUNCTIONAL METHODS
    private Response query(String host, String message){
        try{
            WSocket wSocket = new WSocket();
            wSocket.startConnection(host, 43);
            return new Response (wSocket.send(message));
            // BufferedReader bufferedReader = greenClient.sendMessage(message);
            // return bufferedReader;
        } catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private String nextState(){
        Boolean same_referer = lastResponse.get_referer() == lastReferer;
        Boolean null_referer = lastResponse.get_referer() == null;
        if (same_referer || null_referer) return null;
        return lastResponse.get_referer();
    }

}
