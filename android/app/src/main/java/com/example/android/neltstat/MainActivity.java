package com.example.android.neltstat;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.android.neltstat.RecyclerViewAdapters.ApplicationsRecyclerViewAdapter;
import com.example.android.neltstat.RecyclerViewAdapters.ActiveConnectionsRecyclerViewAdapter;
import com.example.android.neltstat.RecyclerViewAdapters.ConnectionResumeRecyclerViewAdapter;
import com.example.android.neltstat.RecyclerViewAdapters.OrganizationRecyclerViewAdapter;
import com.example.android.neltstat.database.Entities.ActiveConnections;
import com.example.android.neltstat.database.Entities.Applications;
import com.example.android.neltstat.database.Entities.ConnectionsResume;
import com.example.android.neltstat.database.Entities.Organizations;

import java.util.List;

public class MainActivity extends AppCompatActivity{

    private ActiveConnectionsRecyclerViewAdapter activeConnectionsRecyclerViewAdapter;
    private ApplicationsRecyclerViewAdapter      applicationsRecyclerViewAdapter;
    private OrganizationRecyclerViewAdapter      organizationRecyclerViewAdapter;
    private ConnectionResumeRecyclerViewAdapter  connectionResumeRecyclerViewAdapter;

    private MainViewModel mainViewModel;
    private RecyclerView  recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.connections_recycler_view);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        activeConnectionsRecyclerViewAdapter = new ActiveConnectionsRecyclerViewAdapter();
        organizationRecyclerViewAdapter      = new OrganizationRecyclerViewAdapter();
        connectionResumeRecyclerViewAdapter  = new ConnectionResumeRecyclerViewAdapter();
        applicationsRecyclerViewAdapter      = new ApplicationsRecyclerViewAdapter(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mainViewModel.getApplicationsLiveData().observe(this, new Observer<List<Applications>>() {
            @Override
            public void onChanged(@Nullable List<Applications> applicationsList) {
                applicationsRecyclerViewAdapter.setApplicationsArrayList(applicationsList);
            }
        });

        mainViewModel.getActiveConnLiveData().observe(this,new Observer<List<ActiveConnections>>() {
            @Override
            public void onChanged(@Nullable List<ActiveConnections> activeConnectionsList) {
                activeConnectionsRecyclerViewAdapter.setConnectionArrayList(activeConnectionsList);
            }
        });

        mainViewModel.getOrganizationsLiveData().observe(this, new Observer<List<Organizations>>() {
            @Override
            public void onChanged(@Nullable List<Organizations> organizationsList) {
                organizationRecyclerViewAdapter.setOrgSummaryList(organizationsList);
            }
        });

        mainViewModel.getConnResumeLiveData().observe(this, new Observer<List<ConnectionsResume>>() {
            @Override
            public void onChanged(@Nullable List<ConnectionsResume> connectionsResumes) {
                connectionResumeRecyclerViewAdapter.setConnectionArrayList(connectionsResumes);
            }
        });

        recyclerView.setAdapter(activeConnectionsRecyclerViewAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.database_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_action_refresh_database:
                mainViewModel.update_data();
                break;
            case R.id.action_select_applications:
                recyclerView.setAdapter(applicationsRecyclerViewAdapter);
                break;
            case R.id.action_select_organizations:
                recyclerView.setAdapter(organizationRecyclerViewAdapter);
                break;
            case R.id.action_select_active_connections:
                recyclerView.setAdapter(activeConnectionsRecyclerViewAdapter);
                break;
            case R.id.action_select_connections_resume:
                recyclerView.setAdapter(connectionResumeRecyclerViewAdapter);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
