package com.example.android.neltstat.RecyclerViewAdapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.neltstat.R;
import com.example.android.neltstat.database.Entities.Organizations;

import java.util.List;

public class OrganizationRecyclerViewAdapter
    extends RecyclerView.Adapter<OrganizationRecyclerViewAdapter.OrganizationViewHolder> {

    List<Organizations> orgSummaryList;

    @NonNull
    @Override
    public OrganizationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.organization_summary,viewGroup, false);
        OrganizationViewHolder organizationViewHolder = new OrganizationViewHolder(view);
        return organizationViewHolder;
    }

    public void setOrgSummaryList(List<Organizations> orgSummaryList) {
        this.orgSummaryList = orgSummaryList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull OrganizationViewHolder organizationViewHolder, int i) {
        organizationViewHolder.set_properties(orgSummaryList.get(i));
    }

    @Override
    public int getItemCount() {
        if (orgSummaryList == null) return 0;
        return orgSummaryList.size();
    }

    class OrganizationViewHolder
            extends     RecyclerView.ViewHolder{

        private TextView
                orgName_textView,
                netName_textView,
                netType_textView,
                minIP_textView,
                maxIP_textView,
                country_textView,
                contactMail_textView,
                abuseMail_textView;

        public OrganizationViewHolder(@NonNull View itemView) {
            super(itemView);
            this.orgName_textView       = itemView.findViewById( R.id.organization_name );
            this.netName_textView       = itemView.findViewById( R.id.net_name          );
            this.netType_textView       = itemView.findViewById( R.id.net_type          );
            this.minIP_textView         = itemView.findViewById( R.id.min_ip            );
            this.maxIP_textView         = itemView.findViewById( R.id.max_ip            );
            this.country_textView       = itemView.findViewById( R.id.country           );
            this.contactMail_textView   = itemView.findViewById( R.id.contact_mail      );
            this.abuseMail_textView     = itemView.findViewById( R.id.abuse_mail        );
        }

        public void set_properties(Organizations organizations
        ){

            orgName_textView    .setText(organizations.getOrg_name()                );
            netName_textView    .setText(organizations.getNet_name()                );
            netType_textView    .setText(organizations.getNet_type()                );
            minIP_textView      .setText(organizations.getMin_ip().getHostAddress() );
            maxIP_textView      .setText(organizations.getMax_ip().getHostAddress() );
            country_textView    .setText(organizations.getCountry()                 );
            contactMail_textView.setText(organizations.getContact_email()           );
            abuseMail_textView  .setText(organizations.getAbuse_email()             );

        }

    }


}
