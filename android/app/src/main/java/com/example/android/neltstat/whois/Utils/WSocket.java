package com.example.android.neltstat.whois.Utils;

import com.example.android.neltstat.whois.Intents.HOSTS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class WSocket {

    private java.net.Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private String host;

    public void startConnection(String ip, int port) throws IOException{
        host = ip;
        clientSocket = new java.net.Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    public BufferedReader send(String msg){

        String final_message = "";

        // Arin accepts "n + " as parameter to get the max information possible
        if(host.equals(HOSTS.ARIN)) final_message = "n + ";
        final_message += msg;

        out.println(final_message);
        return in;
    }

    public void close() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

}
