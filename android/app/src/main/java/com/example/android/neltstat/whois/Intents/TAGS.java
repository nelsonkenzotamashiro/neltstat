package com.example.android.neltstat.whois.Intents;

// TODO (012): Gather and Increase list of tags

public class TAGS {

    public static final String[] REFER = {
            "refer"
    };
    public static final String[] ORG_NAME = {
            "Organization",
            "organisation",
            "OrgName",
            "owner"
    };
    public static final String[] IP_RANGE = {
            "NetRange",
            "CIDR",
            "inetnum"
    };
    public static final String[] NETNAME = {
            "NetName",
            "NetHandle",
            "netname",
            "owner-c"
    };
    public static final String[] NET_TYPE = {
            "status",
            "NetType"
    };
    public static final String[] COUNTRY = {
            "country"
    };
    public static final String[] CONTACT = {
            "mail"
    };
    public static final String[] ABUSE = {
            "abuse"
    };

}

