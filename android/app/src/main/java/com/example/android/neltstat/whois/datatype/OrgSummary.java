package com.example.android.neltstat.whois.datatype;

import android.util.Log;

public class OrgSummary {

    private String orgName;
    private String netName;
    private String minIP;
    private String maxIP;
    private String netType;
    private String country;
    private String emailContact;
    private String emailAbuse;

    private OrgSummary(String orgName,
                       String netName,
                       String minIP,
                       String maxIP,
                       String netType,
                       String country,
                       String emailContact,
                       String emailAbuse
    ) {
        this.orgName = orgName;
        this.netName = netName;
        this.minIP = minIP;
        this.maxIP = maxIP;
        this.netType = netType;
        this.country = country;
        this.emailContact = emailContact;
        this.emailAbuse = emailAbuse;
    }

    public static class Builder{
        private String orgName;
        private String netName;
        private String minIP;
        private String maxIP;
        private String netType;
        private String country;
        private String emailContact;
        private String emailAbuse;

        public Builder set_OrgName(String orgName) {
            this.orgName = orgName;
            return this;
        }
        public Builder set_NetName(String netName) {
            this.netName = netName;
            return this;
        }
        public Builder set_MinIP(String minIP) {
            this.minIP = minIP;
            return this;
        }
        public Builder set_MaxIP(String maxIP) {
            this.maxIP = maxIP;
            return this;
        }
        public Builder set_NetType(String netType) {
            this.netType = netType;
            return this;
        }
        public Builder set_Country(String country) {
            this.country = country;
            return this;
        }
        public Builder set_EmailContact(String emailContact) {
            this.emailContact = emailContact;
            return this;
        }
        public Builder set_EmailAbuse(String emailAbuse) {
            this.emailAbuse = emailAbuse;
            return this;
        }

        public OrgSummary build(){
            return new OrgSummary(
                    orgName,
                    netName,
                    minIP,
                    maxIP,
                    netType,
                    country,
                    emailContact,
                    emailAbuse
            );
        }

    }

    public void update(OrgSummary aux_Org_summary) {
        if (aux_Org_summary.orgName      != null)   this.orgName        = aux_Org_summary.orgName;
        if (aux_Org_summary.netName      != null)   this.netName        = aux_Org_summary.netName;
        if (aux_Org_summary.minIP        != null)   this.minIP          = aux_Org_summary.minIP;
        if (aux_Org_summary.maxIP        != null)   this.maxIP          = aux_Org_summary.maxIP;
        if (aux_Org_summary.netType      != null)   this.netType        = aux_Org_summary.netType;
        if (aux_Org_summary.country      != null)   this.country        = aux_Org_summary.country;
        if (aux_Org_summary.emailContact != null)   this.emailContact   = aux_Org_summary.emailContact;
        if (aux_Org_summary.emailAbuse   != null)   this.emailAbuse     = aux_Org_summary.emailAbuse;
    }

    public String getOrgName() {
        return orgName;
    }
    public String getNetName() {
        return netName;
    }
    public String getMinIP() {
        return minIP;
    }
    public String getMaxIP() {
        return maxIP;
    }
    public String getNetType() {
        return netType;
    }
    public String getCountry() {
        return country;
    }
    public String getEmailContact() {
        return emailContact;
    }
    public String getEmailAbuse() {
        return emailAbuse;
    }

    public String toString() {
        return "" +
                this.orgName        + "\n" +
                this.netName        + "\n" +
                this.minIP          + "\n" +
                this.maxIP          + "\n" +
                this.netType        + "\n" +
                this.country        + "\n" +
                this.emailContact   + "\n" +
                this.emailAbuse
                ;
    }

    public void log() {
        Log.d("NELSON","" +
                this.orgName        + "\n" +
                this.netName        + "\n" +
                this.minIP          + "\n" +
                this.maxIP          + "\n" +
                this.netType        + "\n" +
                this.country        + "\n" +
                this.emailContact   + "\n" +
                this.emailAbuse     + "\n"
        );
    }

}
