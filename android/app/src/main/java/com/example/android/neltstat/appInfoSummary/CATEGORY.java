package com.example.android.neltstat.appInfoSummary;

import android.content.pm.ApplicationInfo;

public class CATEGORY {

    /**
     *  ApplicationInfo.CATEGORY_AUDIO = 1
     *  ApplicationInfo.CATEGORY_GAME = 0
     *  ApplicationInfo.CATEGORY_IMAGE = 3
     *  ApplicationInfo.CATEGORY_MAPS = 6
     *  ApplicationInfo.CATEGORY_NEWS = 5
     *  ApplicationInfo.CATEGORY_PRODUCTIVITY = 7
     *  ApplicationInfo.CATEGORY_SOCIAL = 4
     *  ApplicationInfo.CATEGORY_UNDEFINED = -1
     *  ApplicationInfo.CATEGORY_VIDEO = 2
     *
     */
    public static String toString(int category){
        switch (category){

            case ApplicationInfo.CATEGORY_AUDIO:
                return "Audio";

            case ApplicationInfo.CATEGORY_GAME:
                return "Game";

            case ApplicationInfo.CATEGORY_IMAGE:
                return "Image";

            case ApplicationInfo.CATEGORY_MAPS:
                return "Maps";

            case ApplicationInfo.CATEGORY_NEWS:
                return "News";

            case ApplicationInfo.CATEGORY_PRODUCTIVITY:
                return "Productivity";

            case ApplicationInfo.CATEGORY_SOCIAL:
                return "Social";

            case ApplicationInfo.CATEGORY_VIDEO:
                return "Video";

            case ApplicationInfo.CATEGORY_UNDEFINED:
                return "Undefined";

        }
        return "Undefined";
    }

    public static final int CATEGORY_AUDIO          =  1;
    public static final int CATEGORY_GAME           =  0;
    public static final int CATEGORY_IMAGE          =  3;
    public static final int CATEGORY_MAPS           =  6;
    public static final int CATEGORY_NEWS           =  5;
    public static final int CATEGORY_PRODUCTIVITY   =  7;
    public static final int CATEGORY_SOCIAL         =  4;
    public static final int CATEGORY_UNDEFINED      = -1;
    public static final int CATEGORY_VIDEO          =  2;

}
