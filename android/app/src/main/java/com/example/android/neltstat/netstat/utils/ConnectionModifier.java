package com.example.android.neltstat.netstat.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import com.example.android.neltstat.appInfoSummary.AppInfoRetriever;
import com.example.android.neltstat.appInfoSummary.AppSummary;
import com.example.android.neltstat.database.Entities.Applications;
import com.example.android.neltstat.database.NeltstatDatabase;
import com.example.android.neltstat.netstat.datatype.Connection;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * ConnectionModifier is a Visitor to modify Connection objects
 */
public class ConnectionModifier {

    public static NeltstatDatabase neltstatDatabase;

    /**
     *  Sets app_name attribute and app_icon_drawable reference
     *  Requires: Context to access PackageManager
     * @param context
     * @param connectionArrayList
     */
    public static void set_AppInfo(Context context, ArrayList<Connection> connectionArrayList){
        neltstatDatabase = NeltstatDatabase.InstanceOf(context);
        PackageManager packageManager = context.getPackageManager();
        Iterator<Connection> connectionIterator = connectionArrayList.iterator();
        Connection connection;
        while (connectionIterator.hasNext()){
            connection = connectionIterator.next();
            // TODO (016): Properly handle AppInfo database storage inside a manager class
//            new Thread( new InsertAppSummary(AppInfoRetriever.get_appSummary(context, connection.getUid())) ).start();
//            AppInfoRetriever.get_appSummary(context, connection.getUid()).log();
            try {
                connection.setApp_name(packageManager.getNameForUid(connection.getUid()));
                connection.setAppIconDrawable(
                        packageManager.getApplicationIcon(
                                connection.getApp_name()
                        ));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    /**
     *  Configures remote server info such as organization name.
     *  Requires: Context to access LoaderManager and make queries to Whois servers
     */
    public static void set_OrgInfo(Context context, ArrayList<Connection> connectionArrayList){
        //  TODO (011): implement organization info setter
        return;
    }

    static class InsertAppSummary implements Runnable{

        AppSummary appSummary;

        public InsertAppSummary(AppSummary appSummary) {
            this.appSummary = appSummary;
        }

        @Override
        public void run() {
            appSummary.log();
            Applications applications_fromdb = neltstatDatabase.applicationDAO().load_byUID(Integer.parseInt(appSummary.getApp_uid()));
            Applications applications_new    = new Applications(appSummary, new Date());
            if (applications_fromdb == null){
                Log.d("NELSON", "Inserting app on database");
                neltstatDatabase.applicationDAO().insert(applications_new);
            }
            else  {
                Log.d("NELSON", "Updating app on database");
                applications_fromdb.update(applications_new);
                neltstatDatabase.applicationDAO().update(applications_fromdb);
            }
        }

    }

}
