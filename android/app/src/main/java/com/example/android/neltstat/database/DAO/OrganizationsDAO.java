package com.example.android.neltstat.database.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.android.neltstat.database.Entities.Organizations;
import com.google.common.net.InetAddresses;

import java.net.InetAddress;
import java.util.List;

@Dao
public interface OrganizationsDAO {

    @Insert
    void insert(Organizations... organizations);
    @Insert
    void insert(List<Organizations> organizations);

    @Update
    void update(Organizations... organizations);
    @Update
    void update(List<Organizations> organizations);

    @Delete
    void delete(Organizations... organizations);
    @Delete
    void delete(List<Organizations> organizations);

    @Query("select * from organizations;")
    LiveData<List<Organizations>> load_all();
    @Query("SELECT * FROM organizations WHERE max_ip > :ip AND min_ip < :ip;")
    List<Organizations> load_byIP(InetAddress ip);
    @Query("SELECT * FROM organizations WHERE max_ip > :ip AND min_ip < :ip LIMIT 1;")
    Organizations load_singleByIP(InetAddress ip);
    @Query("SELECT * FROM organizations WHERE min_ip < (:low_ip + :high_ip)/2 AND max_ip > (:low_ip + :high_ip)/2;")
    List<Organizations> load_byRange(InetAddress low_ip, InetAddress high_ip);

}
