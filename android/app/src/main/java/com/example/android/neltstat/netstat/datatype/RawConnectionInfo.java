package com.example.android.neltstat.netstat.datatype;

public class RawConnectionInfo {

    private String localIP;
    private String remoteIP;
    private String localPort;
    private String remotePort;
    private String uid;
    private String stateCode;
    private int    protocol;

    public RawConnectionInfo(
            String localIP,
            String remoteIP,
            String localPort,
            String remotePort,
            String uid,
            String stateCode,
            int protocol
    ) {
        this.localIP = localIP;
        this.remoteIP = remoteIP;
        this.localPort = localPort;
        this.remotePort = remotePort;
        this.uid = uid;
        this.stateCode = stateCode;
        this.protocol  = protocol;
    }

    public static class Builder{

        private String localIP;
        private String remoteIP;
        private String localPort;
        private String remotePort;
        private String uid;
        private String stateCode;
        private int    protocol;


        public Builder set_localIP(String clientIP){
            this.localIP = clientIP;
            return this;
        }
        public Builder set_remoteIP(String serverIP){
            this.remoteIP = serverIP;
            return this;
        }
        public Builder set_localPort(String clientPort){
            this.localPort = clientPort;
            return this;
        }
        public Builder set_remotePort(String serverPort){
            this.remotePort = serverPort;
            return this;
        }
        public Builder set_uid(String uid){
            this.uid = uid;
            return this;
        }
        public Builder set_stateCode(String stateCode){
            this.stateCode = stateCode;
            return this;
        }
        public Builder set_protocol(int protocol){
            this.protocol = protocol;
            return this;
        }
        public RawConnectionInfo build(){
            return new RawConnectionInfo(
                    localIP,
                    remoteIP,
                    localPort,
                    remotePort,
                    uid,
                    stateCode,
                    protocol);
        }

    }

    public String getLocalIP() {
        return localIP;
    }

    public String getRemoteIP() {
        return remoteIP;
    }

    public String getLocalPort() {
        return localPort;
    }

    public String getRemotePort() {
        return remotePort;
    }

    public String getUid() {
        return uid;
    }

    public String getStateCode() {
        return stateCode;
    }

    public int getProtocol() {
        return protocol;
    }
}
