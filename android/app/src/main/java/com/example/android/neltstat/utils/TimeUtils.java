package com.example.android.neltstat.utils;

import java.util.Date;

public class TimeUtils {

    public static String timeDiff(Date first_date, Date last_date){

        long millisseconds = last_date.getTime() - first_date.getTime();
        long seconds       = millisseconds  /1000;
        long minutes       = seconds        /60;
        long hours         = minutes        /60;

        seconds = seconds   %60;
        minutes = minutes   %60;
        hours   = hours     %24;

        String   hours_toFormat = String.valueOf(hours  );
        String minutes_toFormat = String.valueOf(minutes);
        String seconds_toFormat = String.valueOf(seconds);
        String format = "";

        if (  hours_toFormat.length() == 1) format+="0"+hours_toFormat;
        else                                format+=    hours_toFormat;
        format+=":";

        if (minutes_toFormat.length() == 1) format+="0"+minutes_toFormat;
        else                                format+=    minutes_toFormat;
        format+=":";

        if (seconds_toFormat.length() == 1) format+="0"+seconds_toFormat;
        else                                format+=    seconds_toFormat;

        return format;
    }

}
