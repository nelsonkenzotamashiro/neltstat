package com.example.android.neltstat.appInfoSummary;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

public class AppInfoRetriever {

    public static AppSummary get_appSummary(Context context, int uid){
        PackageManager packageManager = context.getPackageManager();
        String packageName = packageManager.getNameForUid(uid);
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(
                    packageName,
                    0);
            return new AppSummary.Builder()
                    .setFullname(packageName)
                    .setName((String) packageManager.getApplicationLabel(applicationInfo))
                    .setVersion(packageManager.getPackageInfo(packageName,0).versionName)
                    .setCategory(CATEGORY.toString(CATEGORY.CATEGORY_UNDEFINED))
                    .setApp_uid(String.valueOf(uid))
                    .build();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return new AppSummary.Builder()
                    .setFullname(packageName)
                    .setName(packageName)
                    .setApp_uid(String.valueOf(uid))
                    .build();
        }
    }

}
