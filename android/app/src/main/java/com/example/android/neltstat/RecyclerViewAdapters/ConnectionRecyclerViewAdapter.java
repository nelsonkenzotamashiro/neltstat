package com.example.android.neltstat.RecyclerViewAdapters;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.neltstat.R;
import com.example.android.neltstat.netstat.datatype.Connection;
import com.example.android.neltstat.netstat.Intents.PROTOCOL;
import com.example.android.neltstat.netstat.Intents.STATE_CODE;

import java.util.ArrayList;

public class ConnectionRecyclerViewAdapter
        extends RecyclerView.Adapter<ConnectionRecyclerViewAdapter.ConnectionViewHolder>{

    private ArrayList<Connection>    connectionArrayList;
    private ConnectionClickInterface connectionClickInterface;

    public ConnectionRecyclerViewAdapter(ConnectionClickInterface connectionClickInterface) {
        this.connectionClickInterface = connectionClickInterface;
    }

    @NonNull
    @Override
    public ConnectionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                        .from(viewGroup.getContext())
                        .inflate(R.layout.connection,viewGroup, false);
        ConnectionViewHolder connectionViewHolder = new ConnectionViewHolder(view);
        return connectionViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ConnectionViewHolder connectionViewHolder, int i) {
        Connection connection = connectionArrayList.get(i);
        connectionViewHolder.set_properties(
                connection.getApp_name(),
                connection.getClient_ip().getHostAddress()+":"+connection.getClient_port(),
                connection.getServer_ip().getHostAddress()+":"+connection.getServer_port(),
                connection.getActualState(),
                connection.getProtocol(),
                connection.getAppIconDrawable()
        );

    }

    @Override
    public int getItemCount() {
        return connectionArrayList.size();
    }

    public void setConnectionArrayList(ArrayList<Connection> connectionArrayList) {
        this.connectionArrayList = connectionArrayList;
        notifyDataSetChanged();
    }

    public interface ConnectionClickInterface{
        void onClick(String targetHost);
    }

    class ConnectionViewHolder
            extends     RecyclerView.ViewHolder
            implements  View.OnClickListener{

        private TextView
                appNameTextView,
                localInfoTextView,
                remoteInfoTextView,
                stateInfoTextView,
                protocolTextView;

        private ImageView appIconImageView;

        public ConnectionViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            appNameTextView    = itemView.findViewById(R.id.app_name      );
            localInfoTextView  = itemView.findViewById(R.id.local_info    );
            stateInfoTextView  = itemView.findViewById(R.id.state_info    );
            protocolTextView   = itemView.findViewById(R.id.protocol_info );
            remoteInfoTextView = itemView.findViewById(R.id.port_ip_info);
            appIconImageView   = itemView.findViewById(R.id.app_icon      );
        }

        public void set_properties(
                String app_name,
                String localInfo,
                String remoteInfo,
                int stateInfo,
                int protocolInfo,
                Drawable iconDrawable
        ){

            appNameTextView.setText(app_name);
            localInfoTextView.setText(localInfo);
            remoteInfoTextView.setText(remoteInfo);
            stateInfoTextView.setText(STATE_CODE.NAME[stateInfo]);
            protocolTextView.setText(PROTOCOL.NAME[protocolInfo]);

            if (iconDrawable == null)   appIconImageView.setImageResource(R.mipmap.ic_launcher);
            else                        appIconImageView.setImageDrawable(iconDrawable);
        }

        @Override
        public void onClick(View v) {
            connectionClickInterface.onClick(connectionArrayList.get(getAdapterPosition()).getServer_ip().getHostAddress());
        }
    }

}
