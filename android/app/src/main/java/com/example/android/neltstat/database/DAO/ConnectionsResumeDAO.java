package com.example.android.neltstat.database.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.android.neltstat.database.Entities.ConnectionsResume;

import java.util.List;

@Dao
public interface ConnectionsResumeDAO {

    @Insert
    void insert(ConnectionsResume... connectionsResume);

    @Delete
    void delete(ConnectionsResume... connectionsResume);

    @Update
    void update(ConnectionsResume... connectionsResume);

    @Query("select * from connections_resume order by last_seen;")
    LiveData<List<ConnectionsResume>> load_all();

}
