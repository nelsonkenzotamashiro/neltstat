package com.example.android.neltstat.netstat.Intents;

public class PROTOCOL {

    public final static int NULL = 0;
    public final static int TCP4 = 1;
    public final static int TCP6 = 2;
    public final static int UDP4 = 3;
    public final static int UDP6 = 4;

    public static final String[] NAME = {
            "NULL",
            "TCP4",
            "TCP6",
            "UDP4",
            "UDP6",
    };

}
