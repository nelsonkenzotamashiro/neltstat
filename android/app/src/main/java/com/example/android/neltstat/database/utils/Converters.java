package com.example.android.neltstat.database.utils;

import android.arch.persistence.room.TypeConverter;

import com.google.common.net.InetAddresses;

import java.net.InetAddress;
import java.util.Date;

public class Converters {

    @TypeConverter
    public static Date fromTimestamp(Long long_date){
        return new Date(long_date);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date){
        return date.getTime();
    }

    @TypeConverter
    public static int inet_aton(InetAddress ip){
        return InetAddresses.coerceToInteger(ip);
    }

    @TypeConverter
    public static InetAddress inet_ntoa(int ip){
        return InetAddresses.fromInteger(ip);
    }

}