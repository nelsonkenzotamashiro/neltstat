package com.example.android.neltstat.utils;

import java.net.InetAddress;

public class IP {

    public static Boolean is_public(InetAddress ip){
        return !(
                ip.isSiteLocalAddress() ||
                ip.isAnyLocalAddress()  ||
                ip.isLinkLocalAddress() ||
                ip.isLoopbackAddress()  ||
                ip.isMulticastAddress()
        );
    }

    public static Boolean is_whoisSearchable(InetAddress ip){
        return !(
                ip.isAnyLocalAddress()
        );
    }

}
