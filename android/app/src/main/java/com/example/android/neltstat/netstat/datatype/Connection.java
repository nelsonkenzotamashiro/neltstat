package com.example.android.neltstat.netstat.datatype;

import android.graphics.drawable.Drawable;
import android.util.Log;

import com.example.android.neltstat.netstat.Intents.PROTOCOL;
import com.example.android.neltstat.netstat.Intents.STATE_CODE;
import com.example.android.neltstat.netstat.utils.TypeTranslationUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

public class Connection {

    private final static String TAG = "NELSON";
//    private final static String TAG = Connection.class.getSimpleName();

    private InetAddress client_ip;
    private InetAddress server_ip;
    private int client_port;
    private int server_port;

    private boolean established;
    private boolean syncSent;
    private boolean syncReceived;
    private boolean finWait1;
    private boolean finWait2;
    private boolean timeWait;
    private boolean closed;
    private boolean closedWait;
    private boolean lastAck;
    private boolean listening;
    private boolean closing;
    private boolean maxEnd;
    private int actualState;

    private Date first_seen;
    private Date last_seen;
    private int protocol;
    private int uid;
    private String app_name;
    private String org_name;
    private Drawable appIconDrawable;

    public static class Builder{
        //  TODO (007): refactor Builder so that it won't access getters and setters from outter class

        private Connection connection;

        public Builder() {
            this.connection = new Connection();
        }

        public Connection build(){
            connection.setLast_seen(connection.getFirst_seen());
            return connection;
        }

        public Builder set_clientRawIP(String rawHexIP) throws UnknownHostException {
            connection.setClient_ip(
                    InetAddress.getByAddress(
                            TypeTranslationUtils.ipBytes_fromString(rawHexIP)
                    )
            );
            return this;
        }

        public Builder set_serverRawIP(String rawHexIP) throws UnknownHostException {
            connection.setServer_ip(
                    InetAddress.getByAddress(
                            TypeTranslationUtils.ipBytes_fromString(rawHexIP)
                    )
            );
            return this;
        }

        public Builder set_clientRawPort(String port){
            connection.setClient_port(
                    TypeTranslationUtils.port_fromHex(port)
            );
            return this;
        }

        public Builder set_serverRawPort(String port){
            connection.setServer_port(
                    TypeTranslationUtils.port_fromHex(
                            port
                    )
            );
            return this;
        }

        public Builder set_rawUid(String uid){
            connection.setUid(TypeTranslationUtils.uid(uid));
            return this;
        }

        public Builder set_rawSeenState(String stateCode){
            connection.setActualState(TypeTranslationUtils.stateCode(stateCode));
            switch (TypeTranslationUtils.stateCode(stateCode)){
                case STATE_CODE.TCP_ESTABLISHED :
                    connection.setEstablished(true);
                    break;
                case STATE_CODE.TCP_SYN_SENT    :
                    connection.setSyncSent(true);
                    break;
                case STATE_CODE.TCP_SYN_RECV    :
                    connection.setSyncReceived(true);
                    break;
                case STATE_CODE.TCP_FIN_WAIT1   :
                    connection.setFinWait1(true);
                    break;
                case STATE_CODE.TCP_FIN_WAIT2   :
                    connection.setFinWait2(true);
                    break;
                case STATE_CODE.TCP_TIME_WAIT   :
                    connection.setTimeWait(true);
                    break;
                case STATE_CODE.TCP_CLOSE       :
                    connection.setClosed(true);
                    break;
                case STATE_CODE.TCP_CLOSE_WAIT  :
                    connection.setClosedWait(true);
                    break;
                case STATE_CODE.TCP_LAST_ACK    :
                    connection.setLastAck(true);
                    break;
                case STATE_CODE.TCP_LISTEN      :
                    connection.setListening(true);
                    break;
                case STATE_CODE.TCP_CLOSING     :
                    connection.setClosing(true);
                    break;
                case STATE_CODE.TCP_MAX_END     :
                    connection.setMaxEnd(true);
                    break;
            }
            return this;
        }

        public Builder set_protocol(int protocol){
            if      (protocol == PROTOCOL.TCP4) connection.setProtocol(PROTOCOL.TCP4);
            else if (protocol == PROTOCOL.TCP6) connection.setProtocol(PROTOCOL.TCP6);
            else if (protocol == PROTOCOL.UDP4) connection.setProtocol(PROTOCOL.UDP4);
            else if (protocol == PROTOCOL.UDP6) connection.setProtocol(PROTOCOL.UDP6);
            else                                connection.setProtocol(PROTOCOL.NULL);
            return this;
        }

        public Builder set_clientIP(InetAddress ip){
            connection.setClient_ip(ip);
            return this;
        }

        public Builder set_serverIP(InetAddress ip){
            connection.setServer_ip(ip);
            return this;
        }

        public Builder set_clientPort(int port){
            connection.setClient_port(port);
            return this;
        }

        public Builder set_serverPort(int port){
            connection.setServer_port(port);
            return this;
        }

        public Builder set_uid(int uid){
            connection.setUid(uid);
            return this;
        }

        public Builder set_firstSeen(Date date){
            connection.setFirst_seen(date);
            return this;
        }

        public Builder set_seenState(int stateCode){
            switch (stateCode){
                case STATE_CODE.TCP_ESTABLISHED :
                    connection.setEstablished(true);
                    break;
                case STATE_CODE.TCP_SYN_SENT    :
                    connection.setSyncSent(true);
                    break;
                case STATE_CODE.TCP_SYN_RECV    :
                    connection.setSyncReceived(true);
                    break;
                case STATE_CODE.TCP_FIN_WAIT1   :
                    connection.setFinWait1(true);
                    break;
                case STATE_CODE.TCP_FIN_WAIT2   :
                    connection.setFinWait2(true);
                    break;
                case STATE_CODE.TCP_TIME_WAIT   :
                    connection.setTimeWait(true);
                    break;
                case STATE_CODE.TCP_CLOSE       :
                    connection.setClosed(true);
                    break;
                case STATE_CODE.TCP_CLOSE_WAIT  :
                    connection.setClosedWait(true);
                    break;
                case STATE_CODE.TCP_LAST_ACK    :
                    connection.setLastAck(true);
                    break;
                case STATE_CODE.TCP_LISTEN      :
                    connection.setListening(true);
                    break;
                case STATE_CODE.TCP_CLOSING     :
                    connection.setClosing(true);
                    break;
                case STATE_CODE.TCP_MAX_END     :
                    connection.setMaxEnd(true);
                    break;

            }
            return this;
        }

    }

    public void logInfo_simple(){
        Log.d(
                TAG,this.app_name+" "+this.server_ip.getHostAddress()+":"+this.server_port
        );
    }

    public String info_asString(){
        return
             PROTOCOL.NAME[protocol] + " " +
             client_ip.getHostAddress() + ":" + Integer.toString(client_port) + " " +
             server_ip.getHostAddress() + ":" + Integer.toString(server_port);
    }

    public static String getTAG() {
        return TAG;
    }

    public int getProtocol() {
        return protocol;
    }

    public void setProtocol(int protocol) {
        this.protocol = protocol;
    }

    public InetAddress getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(InetAddress client_ip) {
        this.client_ip = client_ip;
    }

    public InetAddress getServer_ip() {
        return server_ip;
    }

    public void setServer_ip(InetAddress server_ip) {
        this.server_ip = server_ip;
    }

    public int getClient_port() {
        return client_port;
    }

    public void setClient_port(int client_port) {
        this.client_port = client_port;
    }

    public int getServer_port() {
        return server_port;
    }

    public void setServer_port(int server_port) {
        this.server_port = server_port;
    }

    public boolean isEstablished() {
        return established;
    }

    public void setEstablished(boolean established) {
        this.established = established;
    }

    public boolean isSyncSent() {
        return syncSent;
    }

    public void setSyncSent(boolean syncSent) {
        this.syncSent = syncSent;
    }

    public boolean isSyncReceived() {
        return syncReceived;
    }

    public void setSyncReceived(boolean syncReceived) {
        this.syncReceived = syncReceived;
    }

    public boolean isFinWait1() {
        return finWait1;
    }

    public void setFinWait1(boolean finWait1) {
        this.finWait1 = finWait1;
    }

    public boolean isFinWait2() {
        return finWait2;
    }

    public void setFinWait2(boolean finWait2) {
        this.finWait2 = finWait2;
    }

    public boolean isTimeWait() {
        return timeWait;
    }

    public void setTimeWait(boolean timeWait) {
        this.timeWait = timeWait;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public boolean isClosedWait() {
        return closedWait;
    }

    public void setClosedWait(boolean closedWait) {
        this.closedWait = closedWait;
    }

    public boolean isLastAck() {
        return lastAck;
    }

    public void setLastAck(boolean lastAck) {
        this.lastAck = lastAck;
    }

    public boolean isListening() {
        return listening;
    }

    public void setListening(boolean listening) {
        this.listening = listening;
    }

    public boolean isClosing() {
        return closing;
    }

    public void setClosing(boolean closing) {
        this.closing = closing;
    }

    public boolean isMaxEnd() {
        return maxEnd;
    }

    public void setMaxEnd(boolean maxEnd) {
        this.maxEnd = maxEnd;
    }

    public Date getFirst_seen() {
        return first_seen;
    }

    public void setFirst_seen(Date first_seen) {
        this.first_seen = first_seen;
    }

    public Date getLast_seen() {
        return last_seen;
    }

    public void setLast_seen(Date last_seen) {
        this.last_seen = last_seen;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setActualState(int actualState) {
        this.actualState = actualState;
    }

    public int getActualState() {
        return actualState;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public Drawable getAppIconDrawable() {
        return appIconDrawable;
    }

    public void setAppIconDrawable(Drawable appIconDrawable) {
        this.appIconDrawable = appIconDrawable;
    }
}
