package com.example.android.neltstat.netstat.Intents;

public class STATE_CODE {

    public static final int TCP_ESTABLISHED = 1;
    public static final int TCP_SYN_SENT    = 2;
    public static final int TCP_SYN_RECV    = 3;
    public static final int TCP_FIN_WAIT1   = 4;
    public static final int TCP_FIN_WAIT2   = 5;
    public static final int TCP_TIME_WAIT   = 6;
    public static final int TCP_CLOSE       = 7;
    public static final int TCP_CLOSE_WAIT  = 8;
    public static final int TCP_LAST_ACK    = 9;
    public static final int TCP_LISTEN      = 10;
    public static final int TCP_CLOSING     = 11;
    public static final int TCP_MAX_END     = 12;

    public static final String[] NAME = {
            null,
            "ESTABLISHED",
            "SYN_SENT",
            "SYN_RECV",
            "FIN_WAIT1",
            "FIN_WAIT2",
            "TIME_WAIT",
            "CLOSE",
            "CLOSE_WAIT",
            "LAST_ACK",
            "LISTEN",
            "CLOSING",
            "MAX_END"
    };

}
