package com.example.android.neltstat.database;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.example.android.neltstat.database.DAO.ActiveConnectionsDAO;
import com.example.android.neltstat.database.DAO.ApplicationsDAO;
import com.example.android.neltstat.database.DAO.ConnectionsResumeDAO;
import com.example.android.neltstat.database.DAO.OrganizationsDAO;
import com.example.android.neltstat.database.Entities.ActiveConnections;
import com.example.android.neltstat.database.Entities.Applications;
import com.example.android.neltstat.database.Entities.ConnectionsResume;
import com.example.android.neltstat.database.Entities.Organizations;
import com.example.android.neltstat.database.utils.Converters;

@android.arch.persistence.room.Database(
        entities = {Applications.class, ActiveConnections.class, Organizations.class, ConnectionsResume.class},
        version = 1,
        exportSchema = false
)
@TypeConverters({Converters.class})
public abstract class NeltstatDatabase extends RoomDatabase{

    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "netstat_db";
    private static NeltstatDatabase instanceOf;

    public static NeltstatDatabase InstanceOf(Context context){
        if (instanceOf == null){
            synchronized (LOCK){
                instanceOf = Room.databaseBuilder(
                        context.getApplicationContext(),
                        NeltstatDatabase.class,
                        DATABASE_NAME
                ).build();
            }
        }
        return instanceOf;
    }

    public void destroy(){
        instanceOf = null;
    }

    public abstract OrganizationsDAO organizationDAO();
    public abstract ApplicationsDAO applicationDAO();
    public abstract ActiveConnectionsDAO activeConnectionsDAO();
    public abstract ConnectionsResumeDAO connectionsResumeDAO();

}
