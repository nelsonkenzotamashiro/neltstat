package com.example.android.neltstat.appInfoSummary;

import android.util.Log;

public class AppSummary {

    private String uid;
    private String name;
    private String fullname;
    private String version;
    private String category;

    public static class Builder{

        private String name;
        private String fullname;
        private String version;
        private String uid;
        private String category;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }
        public Builder setFullname(String fullname) {
            this.fullname = fullname;
            return this;
        }
        public Builder setVersion(String version) {
            this.version = version;
            return this;
        }
        public Builder setApp_uid(String uid) {
            this.uid = uid;
            return this;
        }
        public Builder setCategory(String category) {
            this.category = category;
            return this;
        }

        public AppSummary build (){
            return new AppSummary(uid, name, fullname, version, category);
        }

    }

    private AppSummary(String uid, String name, String fullname, String version, String category) {
        this.uid = uid;
        this.name = name;
        this.fullname = fullname;
        this.version = version;
        this.category = category;
    }

    public String getName() {
        return name;
    }
    public String getFullname() {
        return fullname;
    }
    public String getVersion() {
        return version;
    }
    public String getCategory() {
        return category;
    }
    public String getApp_uid() {
        return uid;
    }

    public void log(){
        Log.d("NELSON", ""+
                uid         +"\n"+
                name        +"\n"+
                fullname    +"\n"+
                category    +"\n"+
                version
        );
    }

    public void update(AppSummary appSummary){
        if (appSummary.getApp_uid () != null) this.uid      = appSummary.getApp_uid ();
        if (appSummary.getName    () != null) this.name     = appSummary.getName    ();
        if (appSummary.getFullname() != null) this.fullname = appSummary.getFullname();
        if (appSummary.getVersion () != null) this.version  = appSummary.getVersion ();
        if (appSummary.getCategory() != null) this.category = appSummary.getCategory();
    }

}
