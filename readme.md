#Introduction
Any machine with access to the Internet has its means to manage TCP/IP connection information: IP addresses, ports, connection state, process ID, user ID. On commons Linux distributions, Netstat command allows the user to list those information quite simply. In Android OS, Netstat command is not easily accessible. It requires CLI access, although the information is equally managed by the OS as Android is built upon a Linux Kernel.
#Description
This repository presents an Android App that implements a netstat-like utility and additional features. It:

* Lists TCP/UDP connections with IPv4 and IPv6 support
* Queries whois servers to retrieve net information (owner, ip range, country, email)
* Lists Internet-connected applications information (app name, version and app icon)
* Stores summarized information locally at Android's SQLite database
* Displays information to the user on RecyclerViews

#Development
##Database
The implementation was driven by the proposed relational database structure. The entities are:

* Connections
* Applications
* Organizations

Applications perform connections and connections are established with organizations.

##Services
It was developed a package to handle data acquisition of each entity from the database. They are:

* Netstat
* Whois
* AppInfoSummary

Data acquisition is currently performed on background through **Executors** and **Runnables**.

###Netstat
The strategy to implement netstat functionality is to retrieve information from the same source as netstat (/proc/net/) and to decode it into useful data.
###Whois
Whois functionality is implemented by querying Iana, Arin, Lacnic, Afrinic, Ripe and Apnic, through **raw sockets on port 43** and resuming the information using an implementation of an Interpreter Pattern.
###AppInfoRetriever
Internet-connected Applications information was retrieved from Android's **PackageManager** using connection's user ID (UID). UID is retrieved from netstat.
##View
Data Presentation is currently implemented through RecyclerViews and RecyclerViewAdapters. There is an implementation of an Adapter for each entity. The same RecyclerView is maintained by the Activity and adapters are just switched when the user wants to switch entity visualization.
##ViewModel
A ViewModel hosts database data implementing **Room** and **LiveData** so that data is observable by the Adapters.
#Known Bugs

* Duplicate data insertion: crashes the app.

#Future Improvements

* Statistics
* Charts
* Search
* Filters

#Disclaim
This application is by no means a hacking application. It is an Information Technology utility! Connections information is managed by Android itself, it belongs to the machine's owner and it is just given to him through a nice user friendly interface. The same is valid to the retrieval of other application information. Whois information on the other hand is public and everybody can access. **No information is exported to public server without user'ip range, country, emailt
