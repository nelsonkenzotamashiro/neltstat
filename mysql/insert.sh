#!/bin/sh

DATABASE="neltstat"
IP_INSERT=$1

IP4_REGEX="[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
# IP6_REGEX="(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))"

WHOIS_GATHER_SH=$(find ../ -name my_whois.sh )

# !WARNING! this method should read from pipe
insert(){
    read ORG_NAME        #="$1"
    read NET_NAME        #="$2"
    read MIN_IP      #="$3"
    read MAX_IP      #="$4"
    read COUNTRY     #="$6"
    read NET_TYPE        #="$5"
    read CONTACT_EMAIL       #="$7"
    read ABUSE_EMAIL     #="$8"
    read LAST_SEEN       #="$9"
    MIN_IP="INET_ATON('$MIN_IP')"
    MAX_IP="INET_ATON('$MAX_IP')"
    ORG_NAME="'$ORG_NAME'"
    COUNTRY="'$COUNTRY'"
    NET_NAME="'$NET_NAME'"
    NET_TYPE="'$NET_TYPE'"
    CONTACT_EMAIL="'$CONTACT_EMAIL'"
    ABUSE_EMAIL="'$ABUSE_EMAIL'"
    LAST_SEEN="DATE('$LAST_SEEN')"

    # echo "INSERT into organization_v4
    #                   (org_name,   net_name,  min_ip,  max_ip,  net_type,  country,  contact_email,  abuse_email,  last_seen )   
    #           VALUES  ($ORG_NAME, $NET_NAME, $MIN_IP, $MAX_IP, $NET_TYPE, $COUNTRY, $CONTACT_EMAIL, $ABUSE_EMAIL, $LAST_SEEN );"
    
    mysql -D "neltstat" -e          \
    "INSERT into organization_v4
                 (org_name,   net_name,  min_ip,  max_ip,  net_type,  country,  contact_email,  abuse_email,  last_seen )   
         VALUES  ($ORG_NAME, $NET_NAME, $MIN_IP, $MAX_IP, $NET_TYPE, $COUNTRY, $CONTACT_EMAIL, $ABUSE_EMAIL, $LAST_SEEN );"
}

query_whois(){
    IP=$1
    bash "$WHOIS_GATHER_SH" "$IP"
}

check_regex(){
    INPUT=$1
    echo "$INPUT" | grep -qE "$IP4_REGEX" && return 0;
    # echo "$INPUT" | grep -qE "$IP6_REGEX" && return 0;
    return 1;
}

include_ips(){
    LIST="$@"
    for ip in ${LIST[@]}; do
        if check_regex $ip; then query_whois "$ip" | insert; fi;
    done
}

include_ips "$@"