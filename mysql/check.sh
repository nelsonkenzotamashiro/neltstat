#!/bin/bash

INPUT=$1

count_entries(){
    IP=$1
    mysql -B -N -D neltstat -e "select count(*) from organization_v4 where min_ip < INET_ATON('$IP') && max_ip > INET_ATON('$IP');"
}

check_entry(){
    IP=$1
    if [[ "$(count_entries $IP)" -gt "0" ]]; then
        return 0; 
    else 
        return 1; 
    fi;
}

check_entry "$INPUT"