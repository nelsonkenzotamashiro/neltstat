#!/bin/bash

REGEX='(^[a-z]*.+abuse.+[a-z]*$)'

assert_regex(){
    INPUT="$@"
    echo $INPUT | grep -q $REGEX        \
        && echo "assert - passed: $@"   \
        || echo "assert - failed: $@"
}

assert_regex ''