#!/bin/bash

# INPUT
TARGET_IP=$1

# HOSTNAMES
IANA____HOST="whois.iana.org"
LACNIC__HOST="whois.lacnic.net"
ARIN____HOST="whois.arin.net"
APNIC___HOST="whois.apnic.net"
AFRINIC_HOST="whois.afrinic.net"
RIPE____HOST="whois.ripe.net"

# TAGS from WHOIS RESPONSE
TAG_REFER="refer"
TAG_OWNER=( "Organization" "organisation" "OrgName" "owner" )
TAG_RANGE=( "NetRange" "CIDR" "inetnum" )
TAG_NETNAME=( "NetName" "NetHandle" "netname" "owner-c" )
TAG_TYPE=( "status" "NetType" )
TAG_COUNTRY=( "country" )
TAG_EMAIL=( "e-mail" "OrgTechEmail" )
TAG_ABUSE=( "e-mail" "OrgAbuseEmail" )

# WHOIS DEFINITIONS
WHOIS_PORT="43"
SERVER_REGEX="whois\.[a-z]{3,8}\.[a-z]{2,3}"

# NECESSARY REGEXES
IP4_REGEX="[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
IP6_REGEX="(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))"
NETMASK_REGEX="/[0-9]{1,3}"

IPV4_REGEX="\b$IP4_REGEX\b"
IPV6_REGEX="\b$IP6_REGEX\b"
NET4_REGEX="\b$IP4_REGEX$NETMASK_REGEX\b"
NET6_REGEX="\b$IP6_REGEX$NETMASK_REGEX\b"

# CHECKS WHETHER RANGE IS DEFINED BY MIN MAX OR BY NETMASK
is_startend(){
    INPUT="$@"; [ ! -t 0 ] && read INPUT;
    IP4_BLOCK=$( echo $INPUT | grep -Eo $IPV4_REGEX )
    IP6_BLOCK=$( echo $INPUT | grep -Eo $IPV6_REGEX )
    if [[ "$(echo "$IP4_BLOCK" | wc -l)" -gt 1 ]]; then return 0; fi;
    if [[ "$(echo "$IP6_BLOCK" | wc -l)" -gt 1 ]]; then return 0; fi;
    return 1;
}

is_block(){
    INPUT="$@"; [ ! -t 0 ] && read INPUT;
    NT4_BLOCK=$( echo $INPUT | grep -Eo $NET4_REGEX )
    NT6_BLOCK=$( echo $INPUT | grep -Eo $NET6_REGEX )
    if [[ "$NT4_BLOCK" != "" || "$NT6_BLOCK" != "" ]]; then return 0; fi;
    return 1;
}

# GETS MIN MAX FROM MIN MAX OR NETMASK
get_rangeFromStartEnd(){
    INPUT="$@"; [ ! -t 0 ] && read INPUT;
    IP4_BLOCK=$( echo $INPUT | grep -Eo $IPV4_REGEX )
    if [[ "$(echo "$IP4_BLOCK" | wc -l)" -gt 1 ]]; then 
        IP_MIN=$(echo $IP4_BLOCK | cut -d ' ' -f 1 )
        IP_MAX=$(echo $IP4_BLOCK | cut -d ' ' -f 2 )
        echo "$IP_MIN $IP_MAX"
        return 0;
    fi;
    IP6_BLOCK=$( echo $INPUT | grep -Eo $IPV6_REGEX )
    if [[ "$(echo "$IP6_BLOCK" | wc -l)" -gt 1 ]]; then
        IP_MIN=$(echo $IP6_BLOCK | cut -d ' ' -f 1 )
        IP_MAX=$(echo $IP6_BLOCK | cut -d ' ' -f 2 )
        echo "$IP_MIN $IP_MAX"
        return 0; 
    fi;
    return 1;
}

get_rangeFromBlock(){
    INPUT="$@"; [ ! -t 0 ] && read INPUT;
    NT4_BLOCK=$( echo $INPUT | grep -Eo $NET4_REGEX )
    NT6_BLOCK=$( echo $INPUT | grep -Eo $NET6_REGEX )
    if [[ "$NT4_BLOCK" != "" ]]; then netmask "$NT4_BLOCK" -r | get_rangeFromStartEnd && return 0; fi;
    if [[ "$NT6_BLOCK" != "" ]]; then netmask "$NT6_BLOCK" -r | get_rangeFromStartEnd && return 0; fi;
    return 1;
}

# REQUESTS INFORMATION FROM WHOIS SERVERS
query(){
    TARGET_IP="$1"
    WHOIS_SERVER="$2"
    # special handle to ARIN params (check site)
    [[ "$WHOIS_SERVER" = "$ARIN____HOST" ]] \
        && RESQUEST_MSG="n + $TARGET_IP"    \
        || RESQUEST_MSG="$TARGET_IP"
    echo "$RESQUEST_MSG" | nc "$WHOIS_SERVER" "$WHOIS_PORT" | tr -dc '[:graph:]\ \n'
}

# GET NET WHOIS SERVER ON REFER TAG AND WHOIS SERVER REGEX
get_refer(){
    RESPONSE="$1"
    echo "$RESPONSE" | grep "^$TAG_REFER:" | grep -Eo "$SERVER_REGEX"
}

get_owner(){
    RESPONSE=$1
    MATCHES=""
    for TAG in ${TAG_OWNER[@]}; do
        # echo "$TAG"
        MATCH=$(echo "$RESPONSE" | grep -nai "^$TAG:" )
        [[ "$MATCH" != "" ]] && MATCHES=$(printf "%s\n%s" $MATCHES $MATCH )
    done
    echo $MATCHES | sort -u | head -n 1 | cut -d ':' -f 3 | awk '{$1=$1};1'
    # echo "$RESPONSE"
}

get_netblock(){
    RESPONSE=$1
    MATCHES=""
    for TAG in ${TAG_RANGE[@]}; do
        # echo "$TAG"
        MATCH=$(echo "$RESPONSE" | grep -nai "^$TAG:" )
        [[ "$MATCH" != "" ]] && MATCHES=$(printf "%s\n%s" $MATCHES $MATCH )
    done
    NET=$(echo $MATCHES | sort -u | head -n 1 | cut -d ':' -f 3 | awk '{$1=$1};1')
    # echo "$RESPONSE"
    is_startend "$NET" && get_rangeFromStartEnd "$NET" && return 0;
    is_block    "$NET" && get_rangeFromBlock    "$NET" && return 0;
}

get_minIP(){
    RESPONSE=$1
    get_netblock "$RESPONSE" | cut -d ' ' -f 1
}

get_maxIP(){
    RESPONSE=$1
    get_netblock "$RESPONSE" | cut -d ' ' -f 2
}

get_netname(){
    RESPONSE=$1
    MATCHES=""
    for TAG in ${TAG_NETNAME[@]}; do
        # echo "$TAG"
        MATCH=$(echo "$RESPONSE" | grep -nai "^$TAG:" | awk '{$1=$1};1' )
        [[ "$MATCH" != "" ]] && MATCHES=$(printf "%s\n\r%s" "$MATCHES" "$MATCH" )
    done
    echo "$MATCHES" | sort -u | head -n 2 | tail -n 1 | cut -d ':' -f 3 | awk '{$1=$1};1'
    # echo "$RESPONSE"
}

get_nettype(){
    RESPONSE=$1
    MATCHES=""
    for TAG in ${TAG_TYPE[@]}; do
        # echo "$TAG"
        MATCH=$(echo "$RESPONSE" | grep -nai "^$TAG:" | awk '{$1=$1};1' )
        [[ "$MATCH" != "" ]] && MATCHES=$(printf "%s\n\r%s" "$MATCHES" "$MATCH" )
    done
    echo "$MATCHES" | sort -u | head -n 2 | tail -n 1 | cut -d ':' -f 3 | awk '{$1=$1};1'
    # echo "$RESPONSE"
}

get_country(){
    RESPONSE=$1
    MATCHES=""
    for TAG in ${TAG_COUNTRY[@]}; do
        # echo "$TAG"
        MATCH=$(echo "$RESPONSE" | grep -nai "^$TAG:" | awk '{$1=$1};1' )
        [[ "$MATCH" != "" ]] && MATCHES=$(printf "%s\n\r%s" "$MATCHES" "$MATCH" )
    done
    echo "$MATCHES" | sort -u | head -n 2 | tail -n 1 | cut -d ':' -f 3 | awk '{$1=$1};1'
    # echo "$RESPONSE"
}

get_email(){
    RESPONSE=$1
    MATCHES=""
    for TAG in ${TAG_EMAIL[@]}; do
        # echo "$TAG"
        MATCH=$(echo "$RESPONSE" | grep -nai "^$TAG:" | awk '{$1=$1};1' )
        [[ "$MATCH" != "" ]] && MATCHES=$(printf "%s\n\r%s" "$MATCHES" "$MATCH" )
    done
    echo "$MATCHES" | sort -u | head -n 2 | tail -n 1 | cut -d ':' -f 3 | awk '{$1=$1};1'
    # echo "$RESPONSE"
}

get_abuse(){
    RESPONSE=$1
    MATCHES=""
    for TAG in ${TAG_ABUSE[@]}; do
        # echo "$TAG"
        MATCH=$(echo "$RESPONSE" | grep -nai "^$TAG:" | awk '{$1=$1};1' )
        [[ "$MATCH" != "" ]] && MATCHES=$(printf "%s\n\r%s" "$MATCHES" "$MATCH" )
    done
    if echo "$MATCHES" | grep -qE '^[a-z]*.+abuse.+[a-z]*$'; then
        echo "$MATCHES" | grep -E '^[a-z]*.+abuse.+[a-z]*$' | head -n 1 | cut -d ':' -f 3 | awk '{$1=$1};1'
        return 0;
    fi;
    echo "$MATCHES" | sort -u | head -n 2 | tail -n 1 | cut -d ':' -f 3 | awk '{$1=$1};1'
    # echo "$RESPONSE"
}

escape_special(){
    INPUT="$@"; [ ! -t 0 ] && read INPUT;
    echo "$INPUT" | iconv -f UTF-8 -t ISO-8859-1//TRANSLIT
}

assert(){
    INPUT="$@"
    eval "$INPUT"                       \
        && echo "assert - passed: $@"   \
        || echo "assert - failed: $@"
}

assert_val(){
    COMMAND="$1"
    ANSWER="$2"
    [[ "$(eval "$COMMAND" )" == "$ANSWER" ]]    \
        && echo "assert - passed: $COMMAND"     \
        || echo "assert - failed: $COMMAND"
}

assert_all(){
    assert "is_startend  '201.0.228.1 - 201.0.228.255'"
    assert "echo '201.0.228.1 - 201.0.228.255' | is_startend"
    assert "is_block '201.0.228.1/24'"
    assert "echo '201.0.228.1/24' | is_block"
    assert_val "get_rangeFromBlock '201.0.228.0/24'"                        "201.0.228.0 201.0.228.255"
    assert_val "echo '201.0.228.0/24' | get_rangeFromBlock"                 "201.0.228.0 201.0.228.255"
    assert_val "get_rangeFromStartEnd '201.0.228.0 - 201.0.228.255'"        "201.0.228.0 201.0.228.255"
    assert_val "echo '201.0.228.0 - 201.0.228.255' | get_rangeFromStartEnd" "201.0.228.0 201.0.228.255"
    assert_val "get_nettype 'status:       ALLOCATED'"                      "ALLOCATED"

}

main_resolve(){
    
    LAST_REFER=""
    LAST_RESPONSE=""
    MAX_QUERIES="2"

    LAST_REFER="$IANA____HOST"
    LAST_RESPONSE=$( query "$TARGET_IP" "$LAST_REFER" )
    OWNER=$( get_owner    "$LAST_RESPONSE" )
    RANGE=$( get_netblock "$LAST_RESPONSE" )
    MINIP=$( get_minIP    "$LAST_RESPONSE" )
    MAXIP=$( get_maxIP    "$LAST_RESPONSE" )
    NNAME=$( get_netname  "$LAST_RESPONSE" )
    NTYPE=$( get_nettype  "$LAST_RESPONSE" )
    CNTRY=$( get_country  "$LAST_RESPONSE" )
    EMAIL=$( get_email    "$LAST_RESPONSE" )
    ABUSE=$( get_abuse    "$LAST_RESPONSE" )

    NEW_REFER=$( get_refer "$LAST_RESPONSE" )
    while [[ "$NEW_REFER" != "" && "$NEW_REFER" != "$LAST_REFER" && "$MAX_QUERIES" -gt "0" ]]; do
        
        LAST_REFER="$NEW_REFER"
        LAST_RESPONSE=$( query "$TARGET_IP" "$LAST_REFER" )
        
        NEW_OWNER=$( get_owner    "$LAST_RESPONSE" )
        NEW_RANGE=$( get_netblock "$LAST_RESPONSE" )
        NEW_MINIP=$( get_minIP    "$LAST_RESPONSE" )
        NEW_MAXIP=$( get_maxIP    "$LAST_RESPONSE" )
        NEW_NNAME=$( get_netname  "$LAST_RESPONSE" )
        NEW_NTYPE=$( get_nettype  "$LAST_RESPONSE" )
        NEW_CNTRY=$( get_country  "$LAST_RESPONSE" )
        NEW_EMAIL=$( get_email    "$LAST_RESPONSE" )
        NEW_ABUSE=$( get_abuse    "$LAST_RESPONSE" )
        [[ "$NEW_OWNER" != "" ]] && OWNER="$NEW_OWNER"
        [[ "$NEW_MINIP" != "" ]] && MINIP="$NEW_MINIP"
        [[ "$NEW_MAXIP" != "" ]] && MAXIP="$NEW_MAXIP"
        [[ "$NEW_RANGE" != "" ]] && RANGE="$NEW_RANGE"
        [[ "$NEW_NNAME" != "" ]] && NNAME="$NEW_NNAME"
        [[ "$NEW_NTYPE" != "" ]] && NTYPE="$NEW_NTYPE"
        [[ "$NEW_CNTRY" != "" ]] && CNTRY="$NEW_CNTRY"
        [[ "$NEW_EMAIL" != "" ]] && EMAIL="$NEW_EMAIL"
        [[ "$NEW_ABUSE" != "" ]] && ABUSE="$NEW_ABUSE"
        
        NEW_REFER=$( get_refer $LAST_RESPONSE )
        MAX_QUERIES=$(( MAX_QUERIES - 1 ))

    done

    echo "$OWNER"
    echo "$NNAME"
    # echo "$RANGE"
    echo "$MINIP"
    echo "$MAXIP"
    echo "$CNTRY"
    echo "$NTYPE"
    echo "$EMAIL"
    echo "$ABUSE"
    date +%F
    
    # echo "$LAST_RESPONSE" >&2
}

main_resolve
# assert_all
