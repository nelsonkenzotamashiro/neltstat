#!/bin/sh

INPUT=$1

WHOIS_IP_QUERY="http://whois.arin.net/rest/ip"

TAG_NAME='.net.name["$"]'
TAG_REGISTRATION_DATE='.net.registrationDate["$"]'
TAG_NET_START='.net.netBlocks.netBlock.startAddress["$"]'
TAG_NET_END='.net.netBlocks.netBlock.endAddress["$"]'
TAG_NET_RANGE="[$TAG_NET_START,$TAG_NET_END]"

# GLOBAL VAR
ANSWER=""

get(){
    PARAMETER=$1
    echo "$ANSWER" | jq $PARAMETER | tr -d '"'
}

query_arin(){
    ANSWER=$(curl "$WHOIS_IP_QUERY/$INPUT" -H "Accept:application/json")
}

net_name(){
    get "$TAG_NAME"
}

net_range(){
    netmask "$(get $TAG_NET_START):$(get $TAG_NET_END)" | tr -d ' '
}

net_reg_date(){
    get "$TAG_REGISTRATION_DATE"
}

main_resolve(){
    query_arin
    echo "Net name          : $(net_name)"
    echo "Net range         : $(net_range)"
    echo "Registration Date : $(net_reg_date)"
}

# curl http://whois.arin.net/rest/ip/201.0.228.1 -H "Accept:application/json" | jq $TAG_NAME
main_resolve